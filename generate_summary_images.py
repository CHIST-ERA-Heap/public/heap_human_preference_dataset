import os
from database_util import get_with_request, create_connection, get_user_id_with_name, \
    get_object_name_with_id
import argparse
import cv2
import math
import numpy as np
import sys


def grasp_to_rectangle(center, angle, length, width, verbose = False):
    if verbose:
        print("from my_grasp_to_grconvnet_rectangle")
        print("center: ", center)
        print("angle: ", angle)
        print("length: ", length)
        print("width: ", width)

    rotation_matrix = np.zeros((2, 2))
    rotation_matrix[0][0] = math.cos(angle)
    rotation_matrix[0][1] = -math.sin(angle)
    rotation_matrix[1][0] = math.sin(angle)
    rotation_matrix[1][1] = math.cos(angle)

    p1 = np.zeros((2, 1))
    p2 = np.zeros((2, 1))
    p3 = np.zeros((2, 1))
    p4 = np.zeros((2, 1))

    p1[0][0] = -int(length / 2)
    p1[1][0] = -int(width / 2)

    p2[0][0] = int(length / 2)
    p2[1][0] = -int(width / 2)

    p3[0][0] = int(length / 2)
    p3[1][0] = int(width / 2)

    p4[0][0] = -int(length / 2)
    p4[1][0] = int(width / 2)

    p1_rotated = np.dot(rotation_matrix, p1)
    p2_rotated = np.dot(rotation_matrix, p2)
    p3_rotated = np.dot(rotation_matrix, p3)
    p4_rotated = np.dot(rotation_matrix, p4)

    p1_recentered = (int(p1_rotated[0][0] + center[1]), int(p1_rotated[1][0] + center[0]))
    p2_recentered = (int(p2_rotated[0][0] + center[1]), int(p2_rotated[1][0] + center[0]))
    p3_recentered = (int(p3_rotated[0][0] + center[1]), int(p3_rotated[1][0] + center[0]))
    p4_recentered = (int(p4_rotated[0][0] + center[1]), int(p4_rotated[1][0] + center[0]))

    # rectangle = np.array([p1_recentered,p2_recentered,p3_recentered,p4_recentered]).reshape((4,2)).astype(int)
    rectangle = np.array([p1_recentered[::-1], p2_recentered[::-1], p3_recentered[::-1], p4_recentered[::-1]]).reshape(
        (4, 2)).astype(int)
    return rectangle

# BGR
def convert_to_rgb(val, minval=0.0, maxval=1.0, colors=[(0, 0, 255), (255, 0, 0), (0, 255, 0)]):
    if val > maxval:
        val = maxval
    EPSILON = sys.float_info.epsilon  # Smallest possible difference.
    # "colors" is a series of RGB colors delineating a series of
    # adjacent linear color gradients between each pair.
    # Determine where the given value falls proportionality within
    # the range from minval->maxval and scale that fractional value
    # by the total number in the "colors" pallette.
    try:
        i_f = float(val - minval) / float(maxval - minval) * (len(colors) - 1)
    except Exception as e:
        print(e)
        i_f = 1.0
    # Determine the lower index of the pair of color indices this
    # value corresponds and its fractional distance between the lower
    # and the upper colors.
    try:
        i, f = int(i_f // 1), i_f % 1  # Split into whole & fractional parts.
    except Exception as e:
        print(e)
        f = 0.0
        i=0
    # Does it fall exactly on one of the color points?
    if f < EPSILON:
        return colors[i]
    else:  # Otherwise return a color within the range between them.
        (r1, g1, b1), (r2, g2, b2) = colors[i], colors[i + 1]
        return (int(r1 + f * (r2 - r1)), int(g1 + f * (g2 - g1)), int(b1 + f * (b2 - b1)))


def generate_image_with_grasps(grasps, cv2_img, destination=None, b_with_rectangles = True, thickness_line=5, default_color=None, b_with_circles=False, normalize_scores=False):
    scores = [g[11] for g in grasps]
    if normalize_scores:
        # norm = np.linalg.norm(scores)
        # scores = scores / norm
        current_maxval = np.max(scores)
        current_minval = np.min(scores)
    else:
        current_minval = 0.0
        current_maxval = 1.0
    # print("minval: ", current_minval)
    # print("minval: ", current_maxval)

    # for grasp in grasps:
    for i in range(len(grasps)):
        grasp = grasps[i]
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        x = grasp[3]
        y = grasp[4]
        angle = grasp[8]
        if default_color is None:
            # color = convert_to_rgb(grasp[11])
            color = convert_to_rgb(scores[i], minval=current_minval, maxval=current_maxval)

        else:
            color = default_color
        length = grasp[9]
        width = grasp[10]


        if b_with_rectangles:
            rectangle = grasp_to_rectangle(center=(y, x),
                                           angle=angle, length=length,
                                           width=width)

            rectangle = rectangle.astype('int32')

            blue = (255, 0, 0)
            yellow = (51, 255, 255)
            purple = (204, 204, 0)
            orange = (0, 128, 255)

            if b_with_circles:
                cv2_img = cv2.circle(cv2_img, (rectangle[0, 1], rectangle[0, 0]), 3, blue, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[1, 1], rectangle[1, 0]), 3, yellow, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[2, 1], rectangle[2, 0]), 3, orange, thickness=1, lineType=4)
                cv2_img = cv2.circle(cv2_img, (rectangle[3, 1], rectangle[3, 0]), 3, purple, thickness=1, lineType=4)
            grasp_thickness = 1
            cv2_img = cv2.line(cv2_img, (rectangle[0, 1], rectangle[0, 0]), (rectangle[1, 1], rectangle[1, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[1, 1], rectangle[1, 0]), (rectangle[2, 1], rectangle[2, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[2, 1], rectangle[2, 0]), (rectangle[3, 1], rectangle[3, 0]),
                               color,
                               grasp_thickness, lineType=4)
            cv2_img = cv2.line(cv2_img, (rectangle[3, 1], rectangle[3, 0]), (rectangle[0, 1], rectangle[0, 0]),
                               color,
                               grasp_thickness, lineType=4)

        rotation_matrix = np.zeros((2, 2))
        rotation_matrix[0][0] = math.cos(angle)
        rotation_matrix[0][1] = -math.sin(angle)
        rotation_matrix[1][0] = math.sin(angle)
        rotation_matrix[1][1] = math.cos(angle)

        p1 = np.zeros((2, 1))
        p2 = np.zeros((2, 1))

        p1[0][0] = -int(length / 2)
        p2[0][0] = int(length / 2)

        p1_rotated = np.dot(rotation_matrix, p1)
        p2_rotated = np.dot(rotation_matrix, p2)
        try:
            p1_recentered = (int(p1_rotated[0][0]) + int(x), int(p1_rotated[1][0]) + int(y))
            p2_recentered = (int(p2_rotated[0][0]) + int(x), int(p2_rotated[1][0]) + int(y))
        except:
            p1_recentered = (int(str(p1_rotated[0][0])) + int(str(x)), int(str(p1_rotated[1][0])) + int(str(y)))
            p2_recentered = (int(str(p2_rotated[0][0])) + int(str(x)), int(str(p2_rotated[1][0])) + int(str(y)))


        cv2_img = cv2.line(cv2_img, p1_recentered, p2_recentered,
                           color, thickness=thickness_line)#todo hardcoded

    if destination is not None:
        print("saving image to ", destination)
        cv2.imwrite(destination, cv2_img)

    return cv2_img


def main(args):
    string_to_replace = args.string_to_replace
    replacement_string = args.replacement_string
    destination = args.destination
    database_path = args.database_path
    user_name = args.user_name
    # images_extension = '_labels.png'
    images_extension = args.images_extension


    if os.path.isfile(database_path):
        conn = create_connection(database_path)
        user_id = get_user_id_with_name(conn, user_name, verbose=False)

        dir_path = destination
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)

        all_scenes = get_with_request(conn, sql_request = args.sql_scene_request)
        counter_scene_object = {}
        for scene in all_scenes:
            '''
            object_id integer NOT NULL,
            path text NOT NULL UNIQUE, 
            rgb_extension text,
            depth_extension text,
            background_path text,
            '''
            object_name = get_object_name_with_id(conn, scene[1], verbose=True)

            print('scene: ', scene)
            rgb_image_path = scene[2]+scene[3]
            if os.path.isfile(rgb_image_path):
                rgb_image = cv2.imread(rgb_image_path)
            else:
                rgb_image_path = rgb_image_path.replace(string_to_replace,replacement_string)
                print('rgb_image_path: ', rgb_image_path, ' exists: ',os.path.isfile(rgb_image_path))
                rgb_image = cv2.imread(rgb_image_path)
            sql_request = 'SELECT * FROM grasps WHERE user_id = ? AND scene_id = ?'
            cur = conn.cursor()
            cur.execute(sql_request, [user_id, scene[0]])
            all_grasps_from_scene = cur.fetchall()
            # destination = dir_path + scene[2].rsplit('/', 1)[-1] + images_extension
            if object_name in counter_scene_object:
                counter_scene_object[object_name]+=1
            else:
                counter_scene_object[object_name]=1
            if len(all_grasps_from_scene) > 0:

                destination = dir_path + object_name+'_'+str(counter_scene_object[object_name])+images_extension
                generate_image_with_grasps(all_grasps_from_scene, rgb_image,destination=destination, b_with_rectangles = True, b_with_circles=False)
        print("counter_scene_object: ", counter_scene_object)

        for object_name in counter_scene_object:
            print(object_name, ' has ', counter_scene_object[object_name],' scenes')

        count_object = len(counter_scene_object)
        print("count_object: ", count_object)
    else:
        print(database_path, ' does not exist')

def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Generate database images')

    # parser.add_argument('--string_to_replace', type=str, help='cf readme Export a sqlite database',
    #                     default='../data_files/cornell_dataset/')
    #
    # parser.add_argument('--replacement_string', type=str, help='cf readme Export a sqlite database',
    #                     default='../../data/data_files/cornell_dataset/')
    #
    # parser.add_argument('--destination', type=str, help='path to the folder where the images will be generated ',
    #                     default='../../data/data_files/cornell_summary/')
    #
    # parser.add_argument('--database_path', type=str, help='path to the sqlite database file that sould be read',
    #                     default='../../data/data_sqlite_files/cornell_sqlite.db')
    #
    parser.add_argument('--string_to_replace', type=str, help='cf readme Export a sqlite database',
                        default='../data/data_files/')

    parser.add_argument('--replacement_string', type=str, help='cf readme Export a sqlite database',
                        default='/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/data_files/')

    parser.add_argument('--destination', type=str, help='path to the folder where the images will be generated ',
                        default='../../data/data_files/live_data_table_summary_serena/')

    parser.add_argument('--database_path', type=str, help='path to the sqlite database file that sould be read',
                        default='../../data/data_sqlite_files/clutter_live_data_table_sqlite.db')

    parser.add_argument('--sql_scene_request', type=str, help='sql request',
                        default='SELECT * FROM scenes' )#WHERE object_id != 1 AND object_id != 2 to skip clutter and unknown objects

    parser.add_argument('--user_name', type=str, help='which user name labels to show  ',
                        default='serena')

    parser.add_argument('--images_extension', type=str, help='the end of the images files to be generated',
                        default='_labels.png')

    return parser.parse_args(argv)


if __name__ == '__main__':


    args = parse_arguments(sys.argv[1:])
    main(args)


