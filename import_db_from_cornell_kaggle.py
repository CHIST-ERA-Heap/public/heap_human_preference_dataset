import os
from database_util import create_sqlite_database, get_user_id_with_name, \
    insert_table, get_object_id_with_name, get_scene_id_with_path
from export_db import depth_txt_to_numpy
import numpy as np
import cv2
import sys
import argparse
import math

def get_raw_depth_patch(depth_data, grasp_position_x, grasp_position_y, image_rotation_angle_degree,
                        dim_grasp_patch=96, dim_grasp_patch_y=None, verbose = False):
    rows, cols, canal = depth_data.shape
    DIM = int(dim_grasp_patch / 2)  # diminsion resulting matrix= DIM*2
    if dim_grasp_patch_y is not None:
        DIM_y = int(dim_grasp_patch_y / 2)
    else:
        DIM_y = DIM

    if verbose:
        print("dim_grasp_patch: ", dim_grasp_patch)
        print("DIM: ", DIM)
        print("DIM_y: ", DIM_y)
        print("depth_data.shape: ", depth_data.shape)

    M = cv2.getRotationMatrix2D((int(grasp_position_x), int(grasp_position_y)), image_rotation_angle_degree, 1)
    dst_d = cv2.warpAffine(depth_data, M, (cols, rows))
    dst_d = dst_d[int(grasp_position_y) - DIM_y:int(grasp_position_y) + DIM_y,
            int(grasp_position_x) - DIM:int(grasp_position_x) + DIM]

    # generate depth grayscale patch
    # cf autolab perception

    where_are_NaNs = np.isnan(dst_d)
    max_d = np.max(dst_d)
    # dst_d[where_are_NaNs] = 0.0
    dst_d[where_are_NaNs] = max_d


    if verbose:
        print("dst_d.shape: ", dst_d.shape)
        print("mean dst_d: ", np.mean(dst_d))

    return dst_d  # raw depth

def get_depth_with_width(grasp_position_x, grasp_position_y, rotation_in_radian, cloud_points, gripper_length_pixel,
                         dim_grasp_patch_y=6, fix_delta=0.00, verbose=False):
    if verbose:
        print("grasp_position_x: ", grasp_position_x)
        print("grasp_position_y: ", grasp_position_y)
        print("rotation_in_radian: ", rotation_in_radian)
        print("gripper_length_pixel: ", gripper_length_pixel)
        print("cloud_points.shape: ", cloud_points.shape)
        print("max(cloud_points): ", np.max(cloud_points))

    if gripper_length_pixel <=5:
        gripper_length_pixel = 5
    points_on_line = get_raw_depth_patch(cloud_points, grasp_position_x, grasp_position_y,
                                        math.degrees(rotation_in_radian), dim_grasp_patch=gripper_length_pixel, dim_grasp_patch_y=dim_grasp_patch_y, verbose=verbose)

    if verbose:
        print("points_on_line: ", points_on_line)
        print("points_on_line.shape: ", points_on_line.shape)
        print("max(points_on_line): ", np.max(points_on_line))
    try:
        depth_min = np.min(points_on_line)
        depth_max = np.max(points_on_line)
    except Exception as e:
        print(e)
        depth_min = np.median(cloud_points)
        depth_max = depth_min

    return depth_min+fix_delta, depth_max+fix_delta

def get_angle(points):
    """
    :return: Angle of the grasp to the horizontal.
    """
    dx = points[1, 1] - points[0, 1]
    dy = points[1, 0] - points[0, 0]
    #return (np.arctan2(-dy, dx) + np.pi/2) % np.pi - np.pi/2

    # def angle_trunc(a):get_angle
    #     while a < 0.0:
    #         a += math.pi * 2
    #     return a
    # return angle_trunc(math.atan2(dy, dx))
    # return (np.arctan2(-dy, dx) + np.pi / 2) % np.pi - np.pi / 2

    angle = math.atan2(dy, dx)
    while (angle > np.pi):
        angle -= 2 * np.pi
    while (angle < -np.pi):
        angle += 2 * np.pi
    if angle > np.pi / 2:
        angle = -np.pi / 2 + (angle - np.pi / 2)

    if angle < -np.pi / 2:
        angle = np.pi / 2 + (angle - -np.pi / 2)

    return angle

def get_center(points):
    """
    :return: Rectangle center point
    """
    return points.mean(axis=0).astype(np.int)

def get_length(points):
    """
    :return: Rectangle length (i.e. along the axis of the grasp)
    """
    dx = points[1, 1] - points[0, 1]
    dy = points[1, 0] - points[0, 0]
    return np.sqrt(dx ** 2 + dy ** 2)

def get_width(points):
    """
    :return: Rectangle width (i.e. perpendicular to the axis of the grasp)
    """
    dy = points[2, 1] - points[1, 1]
    dx = points[2, 0] - points[1, 0]
    return np.sqrt(dx ** 2 + dy ** 2)

def get_grasp_vector_with_rectangle(rectangle):
    # print("grasp :")
    # print(rectangle.shape)
    # print(rectangle)

    angle = get_angle(rectangle)
    width = get_width(rectangle)
    length = get_length(rectangle)
    center = get_center(rectangle)
    return float(center[1]), float(center[0]), float(angle), float(length), float(width)

def _gr_text_to_no(l, offset=(0, 0)):
    """
    Transform a single point from a Cornell file line to a pair of ints.
    :param l: Line from Cornell grasp file (str)
    :param offset: Offset to apply to point positions
    :return: Point [y, x]
    """
    x, y = l.split()
    return [int(round(float(y))) - offset[0], int(round(float(x))) - offset[1]]

def load_rectangles_from_cornell(fname, verbose=False):
    grs = []
    with open(fname) as f:
        while True:
            # Load 4 lines at a time, corners of bounding box.
            p0 = f.readline()
            if not p0:
                break  # EOF
            p1, p2, p3 = f.readline(), f.readline(), f.readline()
            try:
                gr = np.array([
                    _gr_text_to_no(p0),
                    _gr_text_to_no(p1),
                    _gr_text_to_no(p2),
                    _gr_text_to_no(p3)
                ])
                if verbose:
                    print("load_rectangles_from_cornell: ", fname)
                    print("gr: ", gr)

                angle = get_angle(gr)
                width = get_width(gr)
                length = get_length(gr)
                center = get_center(gr)

                rotation_matrix = np.zeros((2, 2))
                rotation_matrix[0][0] = math.cos(angle)
                rotation_matrix[0][1] = -math.sin(angle)
                rotation_matrix[1][0] = math.sin(angle)
                rotation_matrix[1][1] = math.cos(angle)

                p1 = np.zeros((2, 1))
                p2 = np.zeros((2, 1))
                p3 = np.zeros((2, 1))
                p4 = np.zeros((2, 1))

                p1[0][0] = -int(length / 2)
                p1[1][0] = -int(width / 2)

                p2[0][0] = int(length / 2)
                p2[1][0] = -int(width / 2)

                p3[0][0] = int(length / 2)
                p3[1][0] = int(width / 2)

                p4[0][0] = -int(length / 2)
                p4[1][0] = int(width / 2)

                p1_rotated = np.dot(rotation_matrix, p1)
                p2_rotated = np.dot(rotation_matrix, p2)
                p3_rotated = np.dot(rotation_matrix, p3)
                p4_rotated = np.dot(rotation_matrix, p4)

                p1_recentered = (int(p1_rotated[0][0] + center[1]), int(p1_rotated[1][0] + center[0]))
                p2_recentered = (int(p2_rotated[0][0] + center[1]), int(p2_rotated[1][0] + center[0]))
                p3_recentered = (int(p3_rotated[0][0] + center[1]), int(p3_rotated[1][0] + center[0]))
                p4_recentered = (int(p4_rotated[0][0] + center[1]), int(p4_rotated[1][0] + center[0]))

                rectangle = np.array(
                    [p1_recentered[::-1], p2_recentered[::-1], p3_recentered[::-1], p4_recentered[::-1]]).reshape(
                    (4, 2)).astype(int)
                if verbose:
                    print("rectangle: ", rectangle)

                grs.append(rectangle)

            except ValueError:
                # Some files contain weird values.
                continue
    return grs

def main(args):

    path_cornell_folder = args.path_cornell_folder
    database_path = args.database_path
    object_mapping_path = './object_cornell_mapping.txt'
    rgb_extension = 'r.png'
    depth_extension = '.txt'
    neg_extension = 'cneg.txt'
    pos_extension = 'cpos.txt'

    if os.path.isfile(database_path):
        os.remove(database_path)

    if not os.path.isfile(database_path):
        # create a database connection
        print('creating tables')
        conn = create_sqlite_database(database_path)
        user_id = get_user_id_with_name(conn, 'default', verbose=False)

        print('filling tables')
        print('filling objects')
        background_mapping_file = open(object_mapping_path, 'r')
        Lines = list(set(background_mapping_file.readlines()))
        for line in Lines:
            # print("line: ", line)
            words = line.split(' ')
            print('words: ', words)
            path_scene = words[0]
            # if path_scene[0] =='0':
            #     path_scene= path_cornell_folder+path_scene[0:2]+'/pcd'+path_scene[1:]
            # else:
            path_scene=path_cornell_folder+path_scene[0:2]+'/pcd'+path_scene

            name_object = words[2]+'_'+words[1]
            rgb_path = path_scene + rgb_extension
            depth_path = path_scene+depth_extension
            scene_background = path_cornell_folder+'backgrounds/'+words[3][:-1].replace('pcdb_','pcdb')+rgb_extension

            background_exists = os.path.isfile(scene_background)
            depth_exists = os.path.isfile(depth_path)
            rgb_exists = os.path.isfile(rgb_path)
            if background_exists and depth_exists and rgb_exists:
                print('path_scene: ', path_scene)
                print('rgb_path: ', rgb_path)
                print('rgb_path is file ', rgb_exists)
                print('depth_path: ', depth_path)
                print('depth_path is file ', depth_exists)
                print('scene_background: ', scene_background)
                print('scene_background is file ', background_exists)
                current_scene_rgb_image = cv2.imread(rgb_path)
                height, width, channels = current_scene_rgb_image.shape

                if depth_extension == '.txt':
                    with_npy = depth_path.replace('.txt', '.npy')
                    if os.path.isfile(with_npy):
                        current_scene_raw_depth_data = np.load(with_npy)
                    else:
                        # we save the transformed file for cornell because this
                        current_scene_raw_depth_data = depth_txt_to_numpy(depth_path, (height, width))
                        np.save(with_npy, current_scene_raw_depth_data)
                elif depth_extension[-4:] == '.npy':
                    current_scene_raw_depth_data = np.load(depth_path)

                if background_exists and depth_exists and rgb_exists:
                    print('name_object: ', name_object)
                    insert_object = [name_object]
                    insert_table(conn, insert_object, type='objects', verbose=False)
                    id_object = get_object_id_with_name(conn, name_object, verbose=False)
                    print("id_object: ", id_object)

                    insert_scene = [id_object, path_scene, rgb_extension, depth_extension, scene_background ]
                    insert_table(conn, insert_scene, type='scenes', verbose=False)
                    id_scene = get_scene_id_with_path(conn, path_scene, verbose=False)
                    print("id_scene: ", id_scene)

                    pos_file_path = path_scene+pos_extension
                    print("pos_file_path: ", pos_file_path)
                    neg_file_path = path_scene + neg_extension
                    print("neg_file_path: ", neg_file_path)
                    quality_handlabel = [1.0, 0.0]
                    i=0
                    for file_grasp_path in [pos_file_path, neg_file_path]:
                        if os.path.isfile(file_grasp_path):
                            rectangles_array = load_rectangles_from_cornell(file_grasp_path)
                            for rectangle in rectangles_array:
                                '''
                                id integer PRIMARY KEY,
                                scene_id integer NOT NULL,
                                user_id integer NOT NULL,
                                x real NOT NULL,
                                y real NOT NULL,
                                depth real NOT NULL,
                                euler_x real NOT NULL,
                                euler_y real NOT NULL,
                                euler_z real NOT NULL,
                                length real NOT NULL,
                                width real NOT NULL,
                                quality real NOT NULL,
                                FOREIGN KEY (scene_id) REFERENCES scenes (id),
                                FOREIGN KEY (user_id) REFERENCES user (id)
                                '''
                                x,y,angle, length, width, = get_grasp_vector_with_rectangle(rectangle)
                                # print("x: ",x )
                                # print("y: ",y )
                                depth_min, depth_max = get_depth_with_width(grasp_position_x=x,
                                                                            grasp_position_y=y,
                                                                            rotation_in_radian=angle,
                                                                            cloud_points=current_scene_raw_depth_data,
                                                                            gripper_length_pixel=length,
                                                                            verbose=False)
                                print("depth_min: ", depth_min)
                                euler_x = 0.0
                                euler_y = 0.0
                                # euler_z = -1.57 + angle #todo unsure
                                euler_z = angle #todo unsure
                                #todo
                                insert_grasp = [id_scene, user_id, x, y, float(depth_min), euler_x, euler_y, euler_z, length, width, quality_handlabel[i]]

                                insert_grasp.append(0.0)
                                insert_grasp.append(0.0)
                                insert_grasp.append(0.0)
                                insert_grasp.append(0.0)

                                insert_table(conn, insert_grasp, type='grasps', verbose=False)
                        i+=1

    else:
        print(database_path, ' exists already')

def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Import database from kaggle')

    parser.add_argument('--path_cornell_folder', type=str, help='path to the folder where the archive was extracted',
                        default='../../data/data_files/cornell_dataset/')

    parser.add_argument('--database_path', type=str, help='path to the sqlite database file that should be created',
                        default='../../data/data_sqlite_files/cornell_sqlite.db')

    return parser.parse_args(argv)

if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])
    main(args)


