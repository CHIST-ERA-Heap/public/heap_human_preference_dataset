import os
from database_util import create_connection, get_with_request, get_object_name_with_id, insert_table, execute_with_request

import sys
import argparse
import datetime
import csv

def main(args):
    header_object = ['object_name', 'nb_of_scenes', 'grasps_success', 'grasps_failure']
    data_object = []
    header_user = ['user_name', 'positive_grasps', 'negative_grasps', 'time']
    data_user=[]
    # data = [
    #     ['Albania', 28748, 'AL', 'ALB'],
    #     ['Algeria', 2381741, 'DZ', 'DZA'],
    #     ['American Samoa', 199, 'AS', 'ASM'],
    #     ['Andorra', 468, 'AD', 'AND'],
    #     ['Angola', 1246700, 'AO', 'AGO']
    # ]

    string_to_replace = args.string_to_replace
    replacement_string = args.replacement_string

    database_path = args.database_path

    if os.path.isfile(database_path):
        print("database_path: ", database_path, " exists")
    else:
        print("database_path: ", database_path, " does not exists")
        exit()

    conn_gt = create_connection(database_path)

    #creating user pkl/txt
    users = get_with_request(conn_gt, sql_request='SELECT * FROM users')
    print("users:", users)
    print("there is ", len(users), "users (with grasp generators users)")

    if args.id_user_to_use is not None:
        new_users = None
        for user in users:
            id_user = user[0]
            if int(id_user) == int(args.id_user_to_use):
                new_users = [user]

        if new_users is None:
            print('id_user_to_use is not correct')
            return

        users = new_users
    #copying scenes and backgrounds
    all_scenes = get_with_request(conn_gt, "SELECT * FROM scenes")
    all_scenes.sort(key=lambda x: x[1])

    print('scenes: ',all_scenes)
    print("there is ", len(all_scenes), "scenes")

    objects = get_with_request(conn_gt, sql_request='SELECT * FROM objects')
    print('objects: ', objects)
    print("there is ", len(objects), "objects (with objects without scenes)")
    dic_obj_id_obj_name = {}
    for o in objects:
        id_obj = o[0]
        object_name = get_object_name_with_id(conn_gt, id_obj, verbose=False)
        dic_obj_id_obj_name[id_obj] = object_name

    # print("there is ", len(objects), "objects (with unknown)")

    dict_grasp_users_positive =  {}
    dict_grasp_users_negative =  {}
    dict_scene_grasp_positive =  {}
    dict_scene_grasp_negative =  {}
    dict_id_object_nb_of_scene =  {}
    dict_id_object_nb_of_positive_grasps =  {}
    dict_id_object_nb_of_negative_grasps =  {}

    for o in objects:
        dict_id_object_nb_of_positive_grasps[o[0]] = 0
        dict_id_object_nb_of_negative_grasps[o[0]] = 0

        dict_id_object_nb_of_scene[o[0]] = 0
        dict_grasp_users_positive[o[0]] = []
        dict_grasp_users_negative[o[0]] = []


    dict_user_id_scenes_without_1pos_and_1neg_label = {}
    for current_scene in all_scenes:
        # if current_scene[1] not in dict_id_object_nb_of_scene:
        #     dict_id_object_nb_of_scene[current_scene[1]] = 1
        # else:
        dict_id_object_nb_of_scene[current_scene[1]] += 1
        dict_scene_grasp_positive[current_scene[1]] = 0
        dict_scene_grasp_negative[current_scene[1]] = 0

        #copying grasps
        for user in users:
            id_user = user[0]
            if id_user not in dict_user_id_scenes_without_1pos_and_1neg_label:
                dict_user_id_scenes_without_1pos_and_1neg_label[id_user] = []

            GT_grasps_pos = get_with_request(conn_gt, sql_request='SELECT * from grasps where scene_id=' + str(current_scene[0]) + " AND user_id =" + str(id_user)+ " AND quality = 1.0")
            if id_user in dict_grasp_users_positive:
                dict_grasp_users_positive[id_user] += GT_grasps_pos
            else:
                dict_grasp_users_positive[id_user] = GT_grasps_pos

            # dict_grasp_users_positive[id_user] += GT_grasps_pos
            dict_scene_grasp_positive[current_scene[1]] += len(GT_grasps_pos)

            GT_grasps_neg = get_with_request(conn_gt, sql_request='SELECT * from grasps where scene_id=' + str(current_scene[0]) + " AND user_id =" + str(id_user)+ " AND quality = 0.0")
            if id_user in dict_grasp_users_negative:
                dict_grasp_users_negative[id_user] += GT_grasps_neg
            else:
                dict_grasp_users_negative[id_user] = GT_grasps_neg
            dict_scene_grasp_negative[current_scene[1]] += len(GT_grasps_neg)


            if dict_scene_grasp_positive[current_scene[1]] == 0 or dict_scene_grasp_negative[current_scene[1]]==0:
                dict_user_id_scenes_without_1pos_and_1neg_label[id_user].append(current_scene)

    if args.print_label_distribution:
        for user in users:
            id_user = user[0]
            if len(dict_grasp_users_positive[id_user])+len(dict_grasp_users_negative[id_user]) > 0:

                #print(datetime.datetime.fromtimestamp(os.path.getctime(f1)) - datetime.datetime.fromtimestamp(os.path.getctime(f2)))
                # print("created: %s" % time.ctime(os.path.getctime(f2)))
                oldest_data_scene =None
                most_recent_date_scene = None
                # for grasps of user
                for g in dict_grasp_users_positive[id_user]:
                    # print('g: ', g)
                    current_scene_id = g[1]
                    current_object_id = g[16]
                    if current_object_id not in dict_id_object_nb_of_positive_grasps:
                        execute_with_request(conn_gt,
                                            sql_request='DELETE FROM grasps WHERE id=' + str(
                                                               g[0]))
                        g[16] = 1 #unknown
                        insert_grasp = list(g[1:])
                        insert_table(conn_gt, insert_grasp, type='grasps', verbose=False)

                    dict_id_object_nb_of_positive_grasps[current_object_id]+=1

                    current_scene = get_with_request(conn_gt, sql_request='SELECT * from scenes where id=' + str(
                        current_scene_id), verbose=False)[0]

                    # object_name = get_object_name_with_id(conn_gt, current_scene[1], verbose=True)

                    # print('scene: ', current_scene)
                    rgb_image_path = current_scene[2] + current_scene[3]
                    if not os.path.isfile(rgb_image_path):
                        rgb_image_path = rgb_image_path.replace(string_to_replace, replacement_string)

                    current_date = datetime.datetime.fromtimestamp(os.path.getctime(rgb_image_path))
                    if oldest_data_scene is None:
                        oldest_data_scene = current_date

                    #date1 after date2
                    if oldest_data_scene > current_date:
                        oldest_data_scene = current_date

                    if most_recent_date_scene is None:
                        most_recent_date_scene = datetime.datetime.fromtimestamp(os.path.getctime(rgb_image_path))

                    # date1 after date2
                    if most_recent_date_scene < current_date:
                        most_recent_date_scene = current_date

                for g in dict_grasp_users_negative[id_user]:
                    g = list(g)
                    current_scene_id = g[1]
                    current_object_id = g[16]
                    if current_object_id not in dict_id_object_nb_of_negative_grasps:
                        execute_with_request(conn_gt,
                                            sql_request='DELETE FROM grasps WHERE id=' + str(
                                                               g[0]))
                        g[16] = 1 #unknown
                        insert_grasp = list(g[1:])
                        insert_table(conn_gt, insert_grasp, type='grasps', verbose=False)

                    dict_id_object_nb_of_negative_grasps[current_object_id] += 1

                    current_scene = get_with_request(conn_gt, sql_request='SELECT * from scenes where id=' + str(
                        current_scene_id), verbose=False)[0]

                    # object_name = get_object_name_with_id(conn_gt, current_scene[1], verbose=True)

                    # print('scene: ', current_scene)
                    rgb_image_path = current_scene[2] + current_scene[3]
                    if not os.path.isfile(rgb_image_path):
                        rgb_image_path = rgb_image_path.replace(string_to_replace, replacement_string)

                    #     rgb_image = cv2.imread(rgb_image_path)
                    # else:
                    #     rgb_image_path = rgb_image_path.replace(string_to_replace, replacement_string)
                    #     print('rgb_image_path: ', rgb_image_path, ' exists: ', os.path.isfile(rgb_image_path))
                    #     rgb_image = cv2.imread(rgb_image_path)
                    current_date = datetime.datetime.fromtimestamp(os.path.getctime(rgb_image_path))
                    if oldest_data_scene is None:
                        oldest_data_scene = current_date

                    # date1 after date2
                    if oldest_data_scene > current_date:
                        oldest_data_scene = current_date

                    if most_recent_date_scene is None:
                        most_recent_date_scene = datetime.datetime.fromtimestamp(os.path.getctime(rgb_image_path))

                    # date1 after date2
                    if most_recent_date_scene < current_date:
                        most_recent_date_scene = current_date



                print(user[1], ' has ', len(dict_grasp_users_positive[id_user])+len(dict_grasp_users_negative[id_user]), " + positives: ", len(dict_grasp_users_positive[id_user]), " - negatives: ", len(dict_grasp_users_negative[id_user]), ' in ', most_recent_date_scene-oldest_data_scene)
                # print('it took: ', )
                #header_user = ['user', 'positive_grasps', 'negative_grasps', 'time']
                data_user.append([user[1], len(dict_grasp_users_positive[id_user]), len(dict_grasp_users_negative[id_user]), most_recent_date_scene-oldest_data_scene])


    nb_of_object = 0
    # object_names.sort(key=lambda x: x[0])
    objects_with_less_than_two_scenes = []
    objects_with_zero_scenes = []
    for id_obj in dic_obj_id_obj_name:
        if args.print_object_distribution:
            print(dic_obj_id_obj_name[id_obj], " has ", dict_id_object_nb_of_scene[id_obj], " scenes - was grasped successfully: ", dict_id_object_nb_of_positive_grasps[id_obj], 'failed to be grasped: ', dict_id_object_nb_of_negative_grasps[id_obj])
        # print('was grasped successfully: ', dict_id_object_nb_of_positive_grasps[id_obj])
        # print('failed to be grasped: ', dict_id_object_nb_of_negative_grasps[id_obj])
        # header_object = ['object_name', 'nb_of_scenes', 'grasps_success', 'grasps_failure']
        data_object.append([dic_obj_id_obj_name[id_obj], dict_id_object_nb_of_scene[id_obj], dict_id_object_nb_of_positive_grasps[id_obj], dict_id_object_nb_of_negative_grasps[id_obj]])

        if dict_id_object_nb_of_scene[id_obj] > 0:
            nb_of_object+=1
        else:
            objects_with_zero_scenes.append((id_obj, dic_obj_id_obj_name[id_obj]))

        if dict_id_object_nb_of_scene[id_obj] < 2:
            objects_with_less_than_two_scenes.append((id_obj, dic_obj_id_obj_name[id_obj]))


    if args.print_object_distribution:
        print("there is ", nb_of_object, 'object with scenes')
        print("there is ", len(objects) -nb_of_object, 'object without scenes')

    if args.print_objects_with_zero_scenes:
        print('objects_with_zero_scenes: ', objects_with_zero_scenes)

    if args.print_objects_with_less_than_two_scenes:
        print('objects_with_less_than_two_scenes: ', objects_with_less_than_two_scenes)

    if args.print_scenes_witout_1pos_and_1neg_label:
        print('scenes_witout_1pos_and_1neg_label: ', dict_user_id_scenes_without_1pos_and_1neg_label[id_user])

    with open('./user.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(header_user)

        # write multiple rows
        writer.writerows(data_user)

    with open('./object.csv', 'w+', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(header_object)

        # write multiple rows
        writer.writerows(data_object)

def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Get stats from a sql database file')

    parser.add_argument('--string_to_replace', type=str, help='cf readme Export a sqlite database',
                        default='../data/data_files/')

    parser.add_argument('--replacement_string', type=str, help='cf readme Export a sqlite database',
                        default='/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/data_files/')

    parser.add_argument('--database_path', type=str, help='depth extension of the files to read',
                        # default='../../data/data_sqlite_files/online_clutter_live_data_table_sqlite.db')
                        default='../../data/data_sqlite_files/clutter_live_data_table_sqlite.db')

    # parser.add_argument('--id_user_to_use', type=int,
    #                     help='id_user_to_use',
    #                     default=1)

    parser.add_argument('--id_user_to_use', type=int,
                        help='id_user_to_use',
                        default=None)

    parser.add_argument('--print_label_distribution', type=bool,
                        help='if True print how many label (total, positive and negative) that user have ',
                        default=True)

    parser.add_argument('--print_object_distribution', type=bool,
                        help='if True print each objects and their associated number of scenes ',
                        default=True)

    parser.add_argument('--print_objects_with_zero_scenes', type=bool,
                        help='if True print objects that do not have scenes associated to them ',
                        default=False)

    parser.add_argument('--print_objects_with_less_than_two_scenes', type=bool,
                        help='if True print objects that do not have scenes at least 2 scene associated to them ',
                        default=False)

    parser.add_argument('--print_scenes_witout_1pos_and_1neg_label', type=bool,
                        help='if True print scenes that do not have at least 1 positive and 1 negative grasps ',
                        default=False)

    return parser.parse_args(argv)

if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])
    main(args)

