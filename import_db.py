import os
import glob
import pickle
from database_util import create_sqlite_database, get_user_id_with_name, insert_table,\
    get_scene_id_with_path, get_object_id_with_name, create_connection, get_with_request, get_object_name_with_id
import sys
import argparse

#todo defined twice
def load_obj(name):
    # print("name: ", name)
    with open(name, 'rb') as f:
        return pickle.load(f)

def main(args):
    sqlite_destination_path = args.sqlite_destination_path
    input_database_path = args.input_database_path
    default_background_path = args.default_background_path
    rgb_extension = args.rgb_extension
    depth_extension = args.depth_extension

    if os.path.isfile(sqlite_destination_path):
        os.remove(sqlite_destination_path)

    print('creating tables')
    conn = create_sqlite_database(sqlite_destination_path)

    path_to_user = input_database_path+"users/users.pkl"
    if os.path.isfile (path_to_user):
        loaded_users = pickle.load(open(path_to_user, "rb"))
    else:
        loaded_users = []
    print("loaded ", len(loaded_users), ' users')

    dict_users_names = {}
    for user in loaded_users:
        print("user: ", user)
        dict_users_names[user[0]]=user[1]
        insert_user = [user[1]]
        insert_table(conn, insert_user, type='users', verbose=False)

    print("dict_users_names: ", dict_users_names)

    users_conn = get_with_request(conn, sql_request='SELECT * FROM users')
    print("test getting users from conn: ", users_conn)

    dict_users_id = {}
    for user in users_conn:
        print('user: ', user)
        dict_users_id[user[1]] = user[0]

    objects_directory_contents = os.listdir(input_database_path+"scenes/")
    objects_directory_contents.sort()

    for object in objects_directory_contents:
        print("object: ", object)

        insert_object = [object]
        insert_table(conn, insert_object, type='objects', verbose=False)
        id_object = get_object_id_with_name(conn, object, verbose=False)
        print("id_object: ", id_object)

        object_path = input_database_path+"scenes/"+object+"/"
        print("object_path: ", object_path)
        list_of_depth_files = glob.glob(object_path + "*.npy")

        for depth_path in list_of_depth_files:
            print("depth_path: ", depth_path)
            path_without_extension = depth_path.replace(".npy","")
            path_scene = depth_path.replace(object_path,"")
            print('path_scene: ', path_scene)
            root_name = path_scene.replace(".npy","")
            print('root_name : ', root_name)

            old_scene_id = root_name.replace(object+'_',"")
            print('old_scene_id : ', old_scene_id)

            # print("old_scene_id: ", old_scene_id)
            possible_scene_background = input_database_path+"backgrounds/"+old_scene_id+"_background.png"
            # print("possible_scene_background: ", possible_scene_background)
            if not os.path.exists(possible_scene_background):
                possible_scene_background = default_background_path

            path = object_path+object+'_'+old_scene_id
            print('path: ', path)
            if not os.path.exists(path+rgb_extension):
                os.rename(path_without_extension+rgb_extension, path+rgb_extension)
                os.rename(path_without_extension+depth_extension, path+depth_extension)

            insert_scene = [id_object, path, rgb_extension, depth_extension, possible_scene_background]
            print("insert_scene: ", insert_scene)
            insert_table(conn, insert_scene, type='scenes', verbose=False)
            id_scene = get_scene_id_with_path(conn, path, verbose=False)
            print("id_scene: ", id_scene)
            # break
            for user in loaded_users:
                # print('user: ', user[0])
                endswith_IdScene_GT_pkl = [filename for filename in os.listdir(object_path) if filename.endswith((str(old_scene_id)+"_GT.pkl"))]
                grasp_files = [filename for filename in endswith_IdScene_GT_pkl if filename.startswith(str(user[0])+'_')]
                if len(grasp_files) >0:
                    grasp_file = object_path+grasp_files[0]
                    print("grasp_file: ", grasp_file)
                    grasp_file_new = path.replace(object_path,object_path+str(user[0])+'_')+"_GT.pkl"
                    if grasp_file != grasp_file_new:
                        os.rename(grasp_file, grasp_file_new)
                        grasp_file = grasp_file_new
                        print("grasp_file_new: ", grasp_file_new)

                    grasp_file_txt = grasp_file.replace('.pkl', '.txt')
                    # grasp_file_new_txt = grasp_file_new.replace('.pkl', '.txt')
                    if os.path.exists(grasp_file) and not os.path.exists(grasp_file_txt):
                        GT_grasps = load_obj(grasp_file)
                        with open(grasp_file_txt, 'w+') as f:
                            for item in GT_grasps:
                                f.write("%s\n" % str(item))


                    if not os.path.exists(path + rgb_extension):
                        os.rename(path_without_extension + rgb_extension, path + rgb_extension)
                        os.rename(path_without_extension + depth_extension, path + depth_extension)
                        grasp_file = path.replace(object_path, object_path + str(user[0]) + '_') + "_GT.pkl"

                    if os.path.exists(grasp_file):
                        grasps = pickle.load(open(grasp_file, "rb"))
                        for grasp in grasps:
                            print("old grasp: ", grasp)
                            '''
                            id integer PRIMARY KEY,
                            scene_id integer NOT NULL,
                            user_id integer NOT NULL,
                            x real NOT NULL,
                            y real NOT NULL,
                            depth real NOT NULL,
                            euler_x real NOT NULL,
                            euler_y real NOT NULL,
                            euler_z real NOT NULL,
                            length real NOT NULL,
                            width real NOT NULL,
                            quality real NOT NULL,
                            '''
                            # id_scene =
                            previous_user_id = grasp[2]
                            previous_user_name = dict_users_names[previous_user_id]
                            user_id = dict_users_id[previous_user_name]
                            x = grasp[3]
                            y = grasp[4]
                            depth_min = grasp[5]
                            euler_x = grasp[6]
                            euler_y = grasp[7]
                            euler_z = grasp[8]
                            length = grasp[9]
                            width = grasp[10]
                            label = grasp[11]
                            #todo
                            insert_grasp = [id_scene, user_id, x, y, float(depth_min), euler_x, euler_y, euler_z, length,
                                            width, label]
                            print("insert_grasp: ", insert_grasp)
                            insert_table(conn, insert_grasp, type='grasps', verbose=False)
                    else:
                        print("grasp file", grasp_file," not found")


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Import database')

    parser.add_argument('--rgb_extension', type=str, help='rgb extension of the files to read',
                        default='.png')

    parser.add_argument('--depth_extension', type=str, help='depth extension of the files to read',
                        default='.npy')

    parser.add_argument('--default_background_path', type=str, help='default path to the background image',
                        default=None)

    parser.add_argument('--input_database_path', type=str, help='path to the input database folder',
                        default='../../data/data_files/online_clutter_live_data_table/')

    parser.add_argument('--sqlite_destination_path', type=str, help='path to the sqlite database file that should be created',
                        default='../../data/data_sqlite_files/online_clutter_live_data_table_sqlite.db')


    return parser.parse_args(argv)

if __name__ == '__main__':


    args = parse_arguments(sys.argv[1:])
    main(args)


