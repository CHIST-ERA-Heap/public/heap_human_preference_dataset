
import os
from database_util import create_connection, get_with_request, get_object_name_with_id

import sys
import argparse


def main(args):
    database_path = args.database_path
    if os.path.isfile(database_path):
        print("database_path: ", database_path, " exists")
    else:
        print("database_path: ", database_path, " does not exists")
        exit()
    conn_gt = create_connection(database_path)

    #get example
    # get all users
    users = get_with_request(conn_gt, sql_request='SELECT * FROM users')
    print('users: ', users)

    users = get_with_request(conn_gt, sql_request='SELECT * FROM users WHERE id = 1')
    print('users 1: ', users)

    # get all objects
    objects = get_with_request(conn_gt, sql_request='SELECT * FROM objects')
    print('objects: ', objects)
    print('len objects: ', len(objects))

    # get all scenes
    scenes = get_with_request(conn_gt, sql_request='SELECT * FROM scenes')
    print('len scenes: ', len(scenes))

    scenes = get_with_request(conn_gt, sql_request='SELECT * FROM scenes WHERE object_id = 1')
    print('len clutter scenes: ', len(scenes))

    # get all grasps
    grasps = get_with_request(conn_gt, sql_request='SELECT * FROM grasps')
    print('len grasps: ', len(grasps))

    grasps = get_with_request(conn_gt, sql_request='SELECT * FROM grasps where quality = 1.0')
    print('len positive grasps: ', len(grasps))


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Get stats from a sql database file')

    parser.add_argument('--database_path', type=str, help='depth extension of the files to read',
                        default='../../data/data_sqlite_files/our_dataset_sqlite.db')

    return parser.parse_args(argv)

if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])
    main(args)



