# Heap Human Preference Dataset

**Author**: Fleytoux Yoann  
**Contact**: yoann.fleytoux@inria.fr  

# What is the purpose of this repository?
This repository contains the code necessary to manage the data used in our implementation of Data-efficient learning of object-centric grasp preferences, that learns grasp preferences with only a few labels per object and generalizes to new views of this object.
(https://gitlab.inria.fr/CHIST-ERA-Heap/public/latent_space_gp_selector )

The data is organise into the following sql database:

![Alt text](./README_images/sqlite_database.png?raw=true "Title")

The objects we want to grasps are captured by a rgbd camera, they have a name for sole attribute.

    id integer PRIMARY KEY, #unique id
    name text NOT NULL UNIQUE #name of the object

By default all the databases have (1, 'unknown') and (2, 'clutter') objects.
Unknown for scenes with the object unlabelled, Clutter for scenes with multiple objects.
 
***

The scene table store the paths to the rgb and depth files.

    id integer PRIMARY KEY, #unique id
    object_id integer NOT NULL, #unique id of the object pressent in the scene
    path text NOT NULL UNIQUE,  #path to camera files without the extension
    rgb_extension text, #rgb extension (usualy .png)
    depth_extension text, #depth extension  (usualy .npy)
    background_path text,

***
Annotated positives and negatives grasps are given by a user. 

Grasps are represented as rectangles centered on the gripper's center position, rotated according to the orientation, with a width that is equaling the gripper opening, and a height that encodes the tolerance.


in our database grasps have the following attributes:

    id integer PRIMARY KEY, #unique id
    x real NOT NULL, #x gripper's center position in pixels
    y real NOT NULL, #gripper's center position in pixels
    depth real NOT NULL, #depth of the gripper in meters
    euler_x real NOT NULL, #orientation of the gripper, currently always 0 for top-down grasps
    euler_y real NOT NULL, #orientation of the gripper, currently always 0 for top-down grasps
    euler_z real NOT NULL, #orientation of the gripper, in radians
    length real NOT NULL, # gripper opening in pixels
    width real NOT NULL,  # height that encodes the tolerance in pixels
    quality real NOT NULL, # 0 or 1

Note that LGPS deals with top-down grasps with 4 degrees of freedom even though the database can handle 6DOF.

***

The user can represent an expert, a user of the system; its grasps will have a quality of 0 or 1, in order to defined which grasp is favored by the user and which grasps should be avoided.
In other case, users can also be grasps generated by other grasping algorithm.

    id integer PRIMARY KEY, #unique id
    name text NOT NULL UNIQUE #name of the user

By default all the databases have (1, 'default') user.
***

# What you will find in this repository

* **data**: the dataset, i.e., RGB+depth images and annotated grasps organized in a sql database. The grasps are also available as txt and pickles files.
  * .npy -> raw (meters) depth camera data numpy array
  * .png -> rgb image from the camera

* **scripts**: python scripts to import, export and explore the datavase
* **camera_preset**: files and replicate our setup for data collection
***
# Setup for data acquisition

* Robot: [Franka emika panda robot](https://www.franka.de/) 
* Robot firmware: Version 2.1.4
* Camera: [camera RealSense D415](https://www.intelrealsense.com/depth-camera-d415//) 

The camera configuration file can be find in the camera_preset folder.
***
# Dataset overview

| Feature  | Description |
| ------ | ------ |
| Number of scenes | 741 |
| Number of scenes with clutter| 331 |
| Number of grasps | 3389 | 
| Number of good grasps | 1935 | 
| Number of bad grasps | 1454 | 
| Image type | rgb, raw depth |
| Image source size | 1280x720 | 
| Number of objects | 177 |
| Number of objects from YCB| 63 |

![Alt text](./README_images/test.png?raw=true)
***

# Installation
You can use the environnement created for LGPS (https://gitlab.inria.fr/CHIST-ERA-Heap/public/latent_space_gp_selector).

The standalone installation:
Tested with python=3.6

    conda update -n base -c defaults conda
    conda create -n DB_interface python=3.6
    conda activate DB_interface
    ~/anaconda3/envs/DB_interface/bin/pip install --upgrade pip
    conda install -c conda-forge numpy
    conda install opencv

***
# Raw File Structure
Some scene can have None as background_path attribute, in this case there is no background file (example bellow: no IdScene2_background.png)

```angular2html
├── destination_folder #where the raw data is stored
|   |── backgrounds    #where the rgb backgrounds images are stored if there is one
|   |   IdScene1_background.png
|   |   IdScene3_background.png
|   |   ...
|   |── scenes  #the scenes are stored by objects, the folder names being the object name 
|   |   |── ObjectName1
|   |   |   |── ObjectName1_IdScene1.png # raw rgb image
|   |   |   |── ObjectName1_IdScene1.npy # raw depth values
|   |   |   |── IdUser1_ObjectName1_IdScene1_GT.pkl # grasps from user1 saved in a pickle
|   |   |   |── IdUser1_ObjectName1_IdScene1_GT.txt # grasps from user1 saved in a txt file
|   |   |   |── IdUser2_ObjectName1_IdScene1_GT.pkl
|   |   |   |── IdUser2_ObjectName1_IdScene1_GT.txt
|   |   |   |── ObjectName1_IdScene2.png 
|   |   |   |── ...
|   |   |── ObjectName2
|   |   |   |── ObjectName2_IdScene3.png 
|   |   |   |── ...
|   |   |── ...
|   |── users #where the user table is stored
|   |   users.txt #list of users saved in a txt file
|   |   users.pkl #list of users saved in a pickle
└── 
```
***
#  Import another database as a sqlite database:
the import_db.py scripts import folders and texts/pickles files if it fits the previously mentioned file structure.
To import our dataset you can download it here:  https://mybox.inria.fr/d/56ebf474e69e4e429b28/ , in the data_files section.

after extracting it to a directory you can use

python3 import_db.py --input_database_path 'path_to_the_folder/' --sqlite_destination_path 'path_to_the_created_sqlfile'

***
#  Export a sqlite database:

the export_db.py scripts export a sql to folders and texts/pickles files:  

examples.db -> destination_folder

The path attribute in scenes refers to a relative disk emplacement where the rgbd files are stored. if this disk emplacement was changed or you are calling scripts from different emplacement, you might need to adjust where the files should be looked for.
To do so, you can use the 'string_to_replace' and 'replacement_string' arguments required in the python scripts.

Example:
the following scene:

(350, 22, '../data_files/cornell_dataset/04/pcd0427', 'r.png', '.txt', '../data_files/cornell_dataset/backgrounds/pcdb0004r.png')

will look for data in the parent directory relative to where you launch the scripts.
if it's correct string_to_replace='' and replacement_string=''.
if not, you can do something like string_to_replace='../data_files' and replacement_string='/home/user/Documents/data_files'.

***
#  Create a new database:
To add a new database you can match the file structure described in 'Raw File Structure'. 
At the minimum you can match

```angular2html
├── new_scenes_folder
|   |── backgrounds 		        
|   |── scenes 
|   |   |── unknown 
|   |   |   |── foo.png # raw rgb image
|   |   |   |── foo.npy # raw depth values
|   |   |   |── foo2.png 
|   |   |   |── foo2.npy 
|   |   |── ...
|   |── users 
└── 
```
python3 input_database_path.py --input_database_path 'path_to_new_scenes_folder/' --sqlite_destination_path 'path_to_new_scenes_sqlfile'

You can then use our GUI to add you own labels  (https://gitlab.inria.fr/CHIST-ERA-Heap/public/latent_space_gp_selector).

***
#  Explore the database:
##  Check dataset:
You can get the number of scenes, objetcs, grasps, etc... by using the get_stats_db.py script.

python3 get_stats_db.py --database_path 'path_to_the_sql_database_file'

##  Generate summary:
You can genererate the scenes with the labels from a given user using the generate_summary_images.py script.

##  Generate scene grid:
You can genererate grids similar to the one in 'Dataset overview' using the create_grid_summary.py script.

***
#  Interracting with SQL requests:
A few example are provided in using_sql_requests_example.py, you can also check the fuctions i util_database.py.


***
#  Comparing with other dataset:
##cornell kaggle database as a sqlite database
To test on the Cornell Grasping dataset (12.9 GB), use this download link: https://www.kaggle.com/oneoneliu/cornell-grasp

| Feature  | Description |
| ------ | ------ |
| Number of scenes | 885 |
| Number of grasps | 8019 | 
| Number of good grasps | 5110 | 
| Number of bad grasps | 2909 | 
| Image type | rgb, raw depth |
| Image source size | 640x480 | 
| Number of objects | 244 | 

![Alt text](./README_images/test_cornell.png?raw=true)

unzip the downloaded archive to a folder and use the import_db_from_cornell_kaggle.py script (no need to change the file structure).

python3 import_db_from_cornell_kaggle.py --path_cornell_folder 'path_to_new_scenes_folder/' --database_path 'path_to_new_scenes_sqlfile'

***
## References for code or software used
https://github.com/skumra/robotic-grasping
