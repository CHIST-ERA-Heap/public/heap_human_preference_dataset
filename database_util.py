import sqlite3
from sqlite3 import Error, IntegrityError
import os
import numpy as np


sql_create_objects_table = """ CREATE TABLE IF NOT EXISTS objects (
                                    id integer PRIMARY KEY,
                                    name text NOT NULL UNIQUE
                                ); """

# sql_create_background_table = """ CREATE TABLE IF NOT EXISTS backgrounds (
#                                     id integer PRIMARY KEY,
#                                     name text NOT NULL UNIQUE
#                                 ); """

sql_create_scenes_table = """CREATE TABLE IF NOT EXISTS scenes (
                                id integer PRIMARY KEY,
                                object_id integer NOT NULL,
                                path text NOT NULL UNIQUE, 
                                rgb_extension text,
                                depth_extension text,
                                background_path text,

                                FOREIGN KEY (object_id) REFERENCES objects (id)
                            );"""

sql_create_users_table = """ CREATE TABLE IF NOT EXISTS users (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL UNIQUE
                                    ); """

# sql_create_grasps_table = """CREATE TABLE IF NOT EXISTS grasps (
    #                                 id integer PRIMARY KEY,
#                                 scene_id integer NOT NULL,
#                                 user_id integer NOT NULL,
#                                 x real NOT NULL,
#                                 y real NOT NULL,
#                                 angle real NOT NULL,
#                                 length real NOT NULL,
#                                 width real NOT NULL,
#                                 depth real NOT NULL,
#                                 quality_handlabel real NOT NULL,
#                                 quality_generator real NOT NULL,
#                                 quality_experiment real NOT NULL,
#                                 FOREIGN KEY (scene_id) REFERENCES scenes (id),
#                                 FOREIGN KEY (user_id) REFERENCES user (id)
#                             );"""

#The three rotations are in a global frame of reference
sql_create_grasps_table = """CREATE TABLE IF NOT EXISTS grasps (
                                id integer PRIMARY KEY,
                                scene_id integer NOT NULL,
                                user_id integer NOT NULL,
                                x real NOT NULL,
                                y real NOT NULL,
                                depth real NOT NULL,
                                euler_x real NOT NULL,
                                euler_y real NOT NULL,
                                euler_z real NOT NULL,
                                length real NOT NULL,
                                width real NOT NULL,
                                quality real NOT NULL,
                                length_meters real NOT NULL,
                                offset_x real NOT NULL,
                                offset_y real NOT NULL,
                                offset_z real NOT NULL,
                                object_id integer NOT NULL,
                                FOREIGN KEY (scene_id) REFERENCES scenes (id),
                                FOREIGN KEY (user_id) REFERENCES user (id),
                                FOREIGN KEY (object_id) REFERENCES objects (id)

                            );"""

def create_sqlite_database(database_path):
    # create a database connection
    # print('creating tables')
    conn = create_connection(database_path)

    # create tables
    if conn is not None:
        # create objects table
        create_table(conn, sql_create_objects_table)

        # create scenes table
        create_table(conn, sql_create_scenes_table)

        # create users table
        create_table(conn, sql_create_users_table)

        # create grasps table
        create_table(conn, sql_create_grasps_table)
    else:
        print("Error! cannot create the database connection.")
    insert_object = ['unknown']
    insert_table(conn, insert_object, type='objects', verbose=False)

    insert_object = ['clutter']
    insert_table(conn, insert_object, type='objects', verbose=False)

    insert_user = ['default']
    insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['skel']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['edge']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['all']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['CVGrasp']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['Dexnet']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['GGCNN']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['GRCONVNET']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['GPD']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['Superquadrics']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    # insert_user = ['6doFGraspNet']
    # insert_table(conn, insert_user, type='users', verbose=False)
    #
    insert_user = ['success']
    insert_table(conn, insert_user, type='users', verbose=False)

    insert_user = ['failures']
    insert_table(conn, insert_user, type='users', verbose=False)


    return conn


def table_to_txt_file(conn, txt_file_path, type):
    sql_request = 'SELECT * FROM '+type
    cur = conn.cursor()
    cur.execute(sql_request)

    rows = cur.fetchall()
    if os.path.isfile(txt_file_path):
        os.remove(txt_file_path)

    file = open(txt_file_path, "a+")

    for row in rows:
        l = ''
        for value in row:
            l+=str(value)+' '

            print('row: ', row)
        file.writelines(l+'\n')

    file.close()


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

def get_newest_id(conn_gt, table_name):
    get_max_id_from_scene_request = 'SELECT * FROM '+table_name+' ORDER BY id DESC LIMIT 1'
    max_id = get_with_request(conn_gt, sql_request=get_max_id_from_scene_request)[0][0]
    return max_id

def insert_table(conn, data_array, type=None, verbose=False):
    sql_request =  'INSERT INTO '+type#(name,priority,status_id,project_id,begin_date,end_date)
                      #VALUES(?,?,?,?,?,?) '
    b_return = True
    if type=="objects":
        # rows = select_table(conn, data_array, type='objects')
        # # print("insert rows: ",rows)
        # if len(rows)==0:
        #     sql_request += '(name)VALUES(?)'
        #     b_return = False
        sql_request += '(name)VALUES(?)'
        b_return = False
    elif type=="users":
        # rows = select_table(conn, data_array, type='users')
        # # print("insert rows: ",rows)
        # if len(rows) == 0:
        #     sql_request += '(name)VALUES(?)'
        #     b_return = False
        sql_request += '(name)VALUES(?)'
        b_return = False
    elif type=="scenes":
        # scenes = select_table(conn, data_array, type='scenes', verbose=False)
        # print("insert scenes: ",scenes)
        # b_found = False
        # for path in scenes:
        #     path = path[0]
        #     # print("name: ", name)
        #     # print("data_array[1]: ", data_array[1])
        #     # print("name == data_array[1]: ", name == data_array[1])
        #
        #     if path == data_array[1]:
        #         b_found=True
        #         break
        # if not b_found:
        #     '''
        #     object_id integer NOT NULL,
        #     path text NOT NULL,
        #     rgb_extension text,
        #     depth_extension text,
        #     background_path text,
        #     '''
        #     sql_request += '(object_id, path, rgb_extension, depth_extension, background_path)VALUES(?,?,?,?,?)'
        #     b_return = False
        sql_request += '(object_id, path, rgb_extension, depth_extension, background_path)VALUES(?,?,?,?,?)'
        b_return = False
    elif type=="grasps":
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        length_meters real NOT NULL,
        offset_x real NOT NULL,
        offset_y real NOT NULL,
        offset_z real NOT NULL,
        FOREIGN KEY (scene_id) REFERENCES scenes (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        sql_request += '(scene_id, user_id, x, y, depth, euler_x, euler_y, euler_z, length, width, quality, length_meters, offset_x, offset_y, offset_z, object_id)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
        b_return = False

    if b_return:
        print("type not recognized or entry found")
        return

    cur = conn.cursor()
    if verbose:
        print("insert sql_request: ", sql_request, data_array)
    try:
        cur.execute(sql_request, data_array)
        conn.commit()

        return cur.lastrowid
    except IntegrityError as e:
        if verbose:
            print(e)
        return


def select_table(conn, data_array, type=None, verbose=False):
    sql_request =  'SELECT '#* FROM '+type+' WHERE '#(name,priority,status_id,project_id,begin_date,end_date)
                      #VALUES(?,?,?,?,?,?) '
    if type=="objects":
        sql_request+='name FROM objects WHERE '
        sql_request += 'name = ?'
    elif type=="users":
        sql_request+='name FROM users WHERE '
        sql_request += 'name = ?'
    elif type=="scenes":
        '''
        object_id integer NOT NULL,
        path text NOT NULL,
        rgb_extension text,
        depth_extension text,
        background_path text,
        '''
        sql_request+='path FROM scenes WHERE '
        sql_request += 'object_id = ?'
        data_array = [data_array[0]]
    else:
        print("type not recognized")
        return None

    cur = conn.cursor()
    if verbose:
        print("select sql_request: ", sql_request, data_array)
    cur.execute(sql_request, data_array)

    return cur.fetchall()

def execute_with_request(conn, sql_request, verbose=False):
    if verbose:
        print("get_with_request: ", sql_request)
    cur = conn.cursor()
    cur.execute(sql_request)
    conn.commit()

def get_with_request(conn, sql_request = 'SELECT * FROM scenes', verbose=False):
    if verbose:
        print("get_with_request: ", sql_request)
    cur = conn.cursor()
    cur.execute(sql_request)
    all_scenes = cur.fetchall()
    return all_scenes

def get_object_name_with_id(conn, name, verbose=False):
    sql_request =  'SELECT name FROM objects WHERE id = ?'

    cur = conn.cursor()
    if verbose:
        print("select sql_request: ", sql_request, name)
    cur.execute(sql_request, [name])

    return cur.fetchall()[0][0]

def get_object_id_with_name(conn, name, verbose=False):
    sql_request =  'SELECT id FROM objects WHERE name = ?'

    cur = conn.cursor()
    if verbose:
        print("select sql_request: ", sql_request, name)
    cur.execute(sql_request, [name])

    return cur.fetchall()[0][0]

def get_user_id_with_name(conn, name, verbose=False):
    sql_request =  'SELECT id FROM users WHERE name = ?'

    cur = conn.cursor()
    if verbose:
        print("select sql_request: ", sql_request, name)
    cur.execute(sql_request, [name])

    return cur.fetchall()[0][0]

def get_scene_id_with_path(conn, path, verbose=False):
    sql_request =  'SELECT id FROM scenes WHERE path = ?'

    cur = conn.cursor()
    if verbose:
        print("select sql_request: ", sql_request, path)
    cur.execute(sql_request, [path])

    return cur.fetchall()[0][0]

def get_scene_with_path(conn, path, verbose=False):
    sql_request =  'SELECT * FROM scenes WHERE path = ?'

    cur = conn.cursor()
    if verbose:
        print("select sql_request: ", sql_request, path)
    cur.execute(sql_request, [path])

    return cur.fetchall()[0][0]


def find_id_with_pcd(all_scenes, pcd):
    for s in all_scenes:
        if s[2].rsplit('/', 1)[-1] == pcd:
            return s[0]
    return None


def extract_sqlite_grasps_from_old_pkl(index_array, all_scenes, conn_gt, id_user_GT):
    grasps_available_for_training = []
    for g in index_array:
        '''
        start_string:  pcd0379
        grasp_str:  _grasp_0.0_
        end_string:  344_257_1.2490457723982544_9.486832980505138_handlabel
        '''
        # print("g: ", g)
        grasp_available_for_training = []
        start_string, grasp_str, end_string = g.partition("_grasp_1.0_")
        if len(end_string) == 0:
            # looking for neg
            start_string, grasp_str, end_string = g.partition("_grasp_0.0_")
            id = find_id_with_pcd(all_scenes, start_string)
            GT_grasps = get_with_request(conn_gt,
                                         sql_request='SELECT * from grasps where user_id =' + str(
                                             id_user_GT) + ' AND scene_id=' + str(
                                             id) + ' AND quality =' + str(0.0))

            # print("start_string: ", start_string)
            # print("grasp_str: ", grasp_str)
            # print("end_string: ", end_string)
        else:
            id = find_id_with_pcd(all_scenes, start_string)
            GT_grasps = get_with_request(conn_gt,
                                         sql_request='SELECT * from grasps where user_id =' + str(
                                             id_user_GT) + ' AND scene_id=' + str(
                                             id) + ' AND quality =' + str(1.0))

        splitted = end_string.split('_')
        # print("splitted: ", splitted)

        x = int(float(splitted[0]))
        y = int(float(splitted[1]))
        theta = float(splitted[2])

        min_dist = 1000000
        index = -1
        a = (x, y)
        for i in range(len(GT_grasps)):
            b = np.array([GT_grasps[i][3], GT_grasps[i][4]])

            dist = np.linalg.norm(a - b)
            if dist < min_dist:
                min_dist = dist
                index = i

        diff_angle = abs(theta - GT_grasps[index][5])
        diff_x = abs(x - GT_grasps[index][3])
        diff_y = abs(y - GT_grasps[index][4])

        if diff_x > 1.0:  # more than 2 degrees
            print("min_dist: ", min_dist, " dif angle: ", diff_angle, " diff_x: ", diff_x, " diff_y: ", diff_y)
            print("the closest grasp to ", x, y, theta, " is ", GT_grasps[index])

        grasps_available_for_training.append(GT_grasps[index])
    return grasps_available_for_training