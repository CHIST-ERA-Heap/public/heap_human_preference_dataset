import os
from database_util import create_connection, get_with_request, insert_table, create_sqlite_database
import sys
import argparse


def main(args):
    database_path = args.database_path
    new_database_path = args.new_database_path

    if os.path.isfile(database_path):
        print("database_path: ", database_path, " exists")

        conn_gt = create_connection(database_path)
        #create db
        conn_new_gt = create_sqlite_database(new_database_path)

        users = get_with_request(conn_gt, sql_request='SELECT * FROM users')
        print('copying users')
        for user in users:
            insert_user = list(user[1:])
            insert_table(conn_new_gt, insert_user, type='users', verbose=False)

        objects = get_with_request(conn_gt, sql_request='SELECT * FROM objects')
        print('copying objects')
        for object in objects:
            insert_object = list(object[1:])

            insert_table(conn_new_gt, insert_object, type='objects', verbose=False)

        scenes = get_with_request(conn_gt, sql_request='SELECT * FROM scenes')
        dict_scene_id_scene =  {}
        len_scenes = len(scenes)
        count_scenes = 0
        print('copying scenes')
        for scene in scenes:
            count_scenes+=1
            print(count_scenes, '/', len_scenes)

            insert_scene = list(scene[1:])

            insert_table(conn_new_gt, insert_scene, type='scenes', verbose=False)
            dict_scene_id_scene[scene[0]] = scene

        grasps = get_with_request(conn_gt, sql_request='SELECT * FROM grasps')
        print('copying grasps')
        len_grasp = len(grasps)
        count_grasp = 0
        for grasp in grasps:
            count_grasp+=1
            print(count_grasp, '/', len_grasp)
            insert_grasp = list(grasp[1:])
            #length_meters, offset_x, offset_y, offset_z
            # insert_grasp.append(0.0)
            # insert_grasp.append(0.0)
            # insert_grasp.append(0.0)
            # insert_grasp.append(0.0)

            id_object = dict_scene_id_scene[grasp[1]][1]
            insert_grasp.append(id_object)

            insert_table(conn_new_gt, insert_grasp, type='grasps', verbose=False)

    else:
        print("database_path: ", database_path, " does not exists")


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Get stats from a sql database file')

    parser.add_argument('--database_path', type=str, help='depth extension of the files to read',
                        default='../../data/data_sqlite_files_before_objectid/cornell_data_new_database_grasps_candidates_sqlite.db')

    parser.add_argument('--new_database_path', type=str, help='depth extension of the files to read',
                        default='../../data/data_sqlite_files_2/cornell_data_new_database_grasps_candidates_sqlite.db')

    return parser.parse_args(argv)

if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])
    main(args)