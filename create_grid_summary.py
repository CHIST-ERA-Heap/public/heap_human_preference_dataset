import math
import matplotlib.pyplot as plt
import os
import sys
import argparse
import random

def main(args):
    # Config:
    images_dir = args.images_dir
    result_grid_filename = args.result_grid_filename
    result_figsize_resolution = args.result_figsize_resolution

    images_list = os.listdir(images_dir)
    images_count = len(images_list)
    print('Images: ', images_list)
    print('Images count: ', images_count)
    nb_figures = args.max_number_of_figures
    if nb_figures > 0:
        random.shuffle(images_list)
        images_list = images_list[:nb_figures]
        images_count = len(images_list)

    # Calculate the grid size:
    grid_size = math.ceil(math.sqrt(images_count))

    # Create plt plot:
    fig, axes = plt.subplots(grid_size, grid_size, figsize=(result_figsize_resolution, result_figsize_resolution))

    current_file_number = 0
    for image_filename in images_list:
        x_position = current_file_number % grid_size
        y_position = current_file_number // grid_size

        plt_image = plt.imread(images_dir + '/' + images_list[current_file_number])
        axes[x_position, y_position].imshow(plt_image)
        print((current_file_number + 1), '/', images_count, ': ', image_filename)

        current_file_number += 1

    plt.subplots_adjust(left=0.0, right=1.0, bottom=0.0, top=1.0)
    plt.savefig(result_grid_filename)


def parse_arguments(argv):

    parser = argparse.ArgumentParser(description='Create grid summary')

    parser.add_argument('--images_dir', type=str, help='path to dir with images',
                        default='../../data/data_files/live_data_table_summary_serena/')

    # parser.add_argument('--images_dir', type=str, help='path to dir with images',
    #                     default='../../data/data_files/cornell_summary/')

    parser.add_argument('--result_figsize_resolution', type=int, help='result subfigure resolution # 1 = 100px',
                        default=10)

    parser.add_argument('--max_number_of_figures', type=int, help='number of figure to use, -1 -> use all figures',
                        default=36)

    parser.add_argument('--result_grid_filename', type=str, help='path and name of the result image',
                        default='./test.png')

    return parser.parse_args(argv)

if __name__ == '__main__':
    args = parse_arguments(sys.argv[1:])
    main(args)
