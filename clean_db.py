import os
from database_util import create_connection, get_with_request, execute_with_request
import sys
import argparse


def main(args):
    remove_objects_without_scenes = args.remove_objects_without_scenes
    database_path = args.database_path

    if os.path.isfile(database_path):
        print("database_path: ", database_path, " exists")
    else:
        print("database_path: ", database_path, " does not exists")
        exit()

    conn_gt = create_connection(database_path)

    #creating user pkl/txt
    users = get_with_request(conn_gt, sql_request='SELECT * FROM users')
    print("users:", users)
    print("there is ", len(users), "(with grasp generators users)")

    #copying scenes and backgrounds
    all_scenes = get_with_request(conn_gt, "SELECT * FROM scenes")
    all_scenes.sort(key=lambda x: x[1])

    print("there is ", len(all_scenes), "scenes")

    objects = get_with_request(conn_gt, sql_request='SELECT * FROM objects')
    print("there is ", len(objects), "objects (with unknown)")
    for o in objects:
        if o[1] == 'unknown':
            id_unknown = o[0]

    if remove_objects_without_scenes:
        #objects that have scenes
        list_id_objects_with_scenes = []
        for s in all_scenes:
            if s[1] not in list_id_objects_with_scenes:
                list_id_objects_with_scenes.append(s[1])

        list_id_objects = [o[0] for o in objects]
        #objects that do not have scenes
        list_id_objects_to_remove = []
        for id_o in list_id_objects:
            if id_o not in list_id_objects_with_scenes and id_o != id_unknown:
                list_id_objects_to_remove.append(id_o)

        #remove objects that do not have scenes
        for id_o in list_id_objects_to_remove:
            sql_scene_command = 'DELETE from objects WHERE id = ' + str(id_o)
            print(sql_scene_command)
            execute_with_request(conn_gt,sql_request=sql_scene_command)


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Clean database')

    parser.add_argument('--database_path', type=str, help='path to the sqlite database file that should be read',
                        default='../../data/data_sqlite_files/clutter_live_data_table_sqlite.db')

    parser.add_argument('--remove_objects_without_scenes', type=bool,
                        help='if True remove objects that do not have scenes ',
                        default=True)

    return parser.parse_args(argv)

if __name__ == '__main__':
    args = parse_arguments(sys.argv[1:])
    main(args)