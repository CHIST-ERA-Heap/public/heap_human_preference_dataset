import os
from database_util import create_connection, get_with_request, get_object_name_with_id
import numpy as np
import pickle
import cv2
import sys
import argparse
from PIL import Image

def mean_4pts(depth_array_input, h_r, w_r):
    depth_array_resized = np.empty((h_r, w_r))
    for i in range(0,h_r):
        for j in range(0, w_r):
            depth_array_resized[i][j]=(
                depth_array_input[2 * i][2 * j]+
                depth_array_input[2 * i][2 * j + 1]+
                depth_array_input[2 * i + 1][2 * j] +
                depth_array_input[2 * i + 1][2 * j + 1]
            )/4
    depth_array_resized = depth_array_resized.reshape((h_r, w_r, 1))
    return depth_array_resized

def convert_from_cv2_to_image(img: np.ndarray) -> Image:
    return Image.fromarray(img)

def convert_from_image_to_cv2(img: Image) -> np.ndarray:
    # return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
    return np.asarray(img)

def resize_pillow(cv2_image, resized_size_w, resized_size_h):
    cv2_image = convert_from_cv2_to_image(cv2_image)
    newsize = (int(resized_size_w), int(resized_size_h))
    cv2_image = cv2_image.resize(newsize)
    return convert_from_image_to_cv2(cv2_image)

def inpaint( img, missing_value=0):
    """
    Inpaint missing values in depth image.
    :param missing_value: Value to fill in teh depth image.
    """
    # cv2 inpainting doesn't handle the border properly
    # https://stackoverflow.com/questions/25974033/inpainting-depth-map-still-a-black-image-border
    img = cv2.copyMakeBorder(img, 1, 1, 1, 1, cv2.BORDER_DEFAULT)
    mask = (img == missing_value).astype(np.uint8)

    # Scale to keep as float, but has to be in bounds -1:1 to keep opencv happy.
    scale = np.abs(img).max()
    img = img.astype(np.float32) / scale  # Has to be float32, 64 not supported.
    img = cv2.inpaint(img, mask, 1, cv2.INPAINT_NS)

    # Back to original size and value range.
    img = img[1:-1, 1:-1]
    img = img * scale
    return img

def from_pcd(pcd_filename, shape, default_filler=0, index=None):
    """
        Create a depth image from an unstructured PCD file.
        If index isn't specified, use euclidean distance, otherwise choose x/y/z=0/1/2
    """
    img = np.zeros(shape)
    if default_filler != 0:
        img += default_filler

    with open(pcd_filename) as f:
        for l in f.readlines():
            ls = l.split()

            if len(ls) != 5:
                # Not a point line in the file.
                continue
            try:
                # Not a number, carry on.
                float(ls[0])
            except ValueError:
                continue

            i = int(ls[4])
            r = i // shape[1]
            c = i % shape[1]

            if index is None:
                x = float(ls[0])
                y = float(ls[1])
                z = float(ls[2])

                img[r, c] = np.sqrt(x ** 2 + y ** 2 + z ** 2)

            else:
                img[r, c] = float(ls[index])

    return img / 1000.0

def depth_txt_to_numpy(pcd_filename, shape):
    di = from_pcd(pcd_filename, shape)
    di = inpaint(di)
    di_array = np.asarray(di)
    return np.expand_dims(di_array, axis=2)  # check

#todo defined twice
def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

#todo defined twice
def get_rgb_depth_background_with_current_scene(current_scene, verbose=True, string_to_replace='', replacement_string=''):
    base_grasp_name = current_scene[2]
    file_name_color = base_grasp_name + current_scene[3]
    if not os.path.isfile(file_name_color):
        print("before: ", current_scene[2])
        print("string_to_replace: ", string_to_replace)
        base_grasp_name = current_scene[2].replace(string_to_replace, replacement_string)
        print("after: ", base_grasp_name)

        file_name_color = base_grasp_name + current_scene[3]
    if not os.path.isfile(file_name_color):
        print(file_name_color, ' does not exists')
        exit()

    file_name_depth = base_grasp_name + current_scene[4]
    if verbose:
        print("current_scene: ", current_scene)
        print("base_grasp_name: ", base_grasp_name)
        print("file_name_color: ", file_name_color)
        print("file_name_depth_npy: ", file_name_depth)
        print("exists: ", os.path.isfile(file_name_depth))


    current_scene_rgb_image = cv2.imread(file_name_color)
    # print("current_scene[5]: ", current_scene[5])
    current_background_scene_rgb_image_path = current_scene[5]
    if current_background_scene_rgb_image_path is not None:
        # current_background_scene_rgb_image_path = current_scene[5].replace('./', './data_files/')
        if not os.path.isfile(current_background_scene_rgb_image_path):
            print("before: ", current_background_scene_rgb_image_path[2])
            print("string_to_replace: ", string_to_replace)
            current_background_scene_rgb_image_path = current_background_scene_rgb_image_path.replace(string_to_replace, replacement_string)
            print("after: ", current_background_scene_rgb_image_path)

        current_background_scene_rgb_image = cv2.imread(current_background_scene_rgb_image_path)
    else:
        current_background_scene_rgb_image = None

    height, width, channels = current_scene_rgb_image.shape
    if verbose:
        print("current_scene_rgb_image.shape: ", current_scene_rgb_image.shape)

    if current_scene[4] == '.txt':
        # with_npy = file_name_depth.replace('.txt','_depth.npy')
        with_npy = file_name_depth.replace('.txt', '.npy')
        if os.path.isfile(with_npy):
            current_scene_raw_depth_data = np.load(with_npy)
        else:
            # we save the transormed file for cornell because this
            current_scene_raw_depth_data = depth_txt_to_numpy(file_name_depth, (height, width))
            np.save(with_npy, current_scene_raw_depth_data)
    elif current_scene[4][-4:] == '.npy':
        current_scene_raw_depth_data = np.load(file_name_depth)
    else:
        print("no method to load depth extension")
        exit()
    return current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels


def main(args):
    string_to_replace = args.string_to_replace
    replacement_string = args.replacement_string
    resize_factor = args.resize_factor
    resize_width = args.resize_width
    destination = args.destination
    database_path = args.database_path

    if os.path.isfile(database_path):
        print("database_path: ", database_path, " exists")
    else:
        print("database_path: ", database_path, " does not exists")
        exit()

    conn_gt = create_connection(database_path)

    if not os.path.exists(destination):
        print("creating target_folder: ", destination)
        os.mkdir(destination)

    #creating folders corresponding to tables:
    # destination_objects= destination+"objects/"
    # if not os.path.exists(destination_objects):
    #     print("creating target_folder: ", destination_objects)
    #     os.mkdir(destination_objects)

    destination_users = destination + "users/"
    if not os.path.exists(destination_users):
        print("creating target_folder: ", destination_users)
        os.mkdir(destination_users)

    destination_scenes= destination+"scenes/"
    if not os.path.exists(destination_scenes):
        print("creating target_folder: ", destination_scenes)
        os.mkdir(destination_scenes)

    destination_backgrounds= destination+"backgrounds/"
    if not os.path.exists(destination_backgrounds):
        print("creating target_folder: ", destination_backgrounds)
        os.mkdir(destination_backgrounds)

    # destination_grasps= destination+"grasps/"
    # if not os.path.exists(destination_grasps):
    #     print("creating target_folder: ", destination_grasps)
    #     os.mkdir(destination_grasps)


    #creating user pkl/txt
    print("creating user pkl/txt")
    users = get_with_request(conn_gt, sql_request='SELECT * FROM users')
    save_obj(users, destination_users+"users.pkl")
    with open(destination_users+"users.txt", 'w+') as f:
        for item in users:
            f.write("%s\n" % str(item))
    # exit()

    #copying scenes and backgrounds
    print("copying scenes and backgrounds")
    all_scenes = get_with_request(conn_gt, "SELECT * FROM scenes")
    all_scenes.sort(key=lambda x: x[1])
    for current_scene in all_scenes:
        print(current_scene)
        current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels = get_rgb_depth_background_with_current_scene(current_scene, string_to_replace=string_to_replace, replacement_string=replacement_string)
        object_name = get_object_name_with_id(conn_gt, current_scene[1], verbose=False)
        destination_object_name = destination_scenes+object_name+"/"

        if not os.path.exists(destination_object_name):
            print("creating target_folder: ", destination_object_name)
            os.mkdir(destination_object_name)

        if current_background_scene_rgb_image is not None:
            background_rgb_name = destination_backgrounds+str(current_scene[0])+"_background.png"
            print("saving: ", background_rgb_name)
            if resize_factor != 1.0:
                h_r = int(current_background_scene_rgb_image.shape[0] * resize_factor)
                w_r = int(current_background_scene_rgb_image.shape[1] * resize_factor)
                # current_background_scene_rgb_image = cv2.resize(current_background_scene_rgb_image, (w_r, h_r), interpolation=cv2.INTER_NEAREST)
                current_background_scene_rgb_image = resize_pillow(current_background_scene_rgb_image, w_r, h_r)
            cv2.imwrite(background_rgb_name, current_background_scene_rgb_image)
        else:
            print("current_background_scene_rgb_image is None")

        destination_object_rgb = destination_object_name+object_name+"_"
        destination_object_depth = destination_object_name+object_name+"_"
        created=False
        while not created:

            temp = destination_object_rgb+str(current_scene[0])+".png"
            if not os.path.isfile(temp):
                print("saving ", temp)
                if resize_factor != 1.0:
                    h_r = int(current_scene_rgb_image.shape[0] * resize_factor)
                    w_r = int(current_scene_rgb_image.shape[1] * resize_factor)
                    # current_scene_rgb_image = cv2.resize(current_scene_rgb_image, (w_r, h_r),
                    #                                                 interpolation=cv2.INTER_NEAREST)
                    current_scene_rgb_image = resize_pillow(current_scene_rgb_image, w_r, h_r)

                cv2.imwrite(temp, current_scene_rgb_image)
                temp = destination_object_depth+str(current_scene[0])+".npy"
                print("saving ", temp)
                if resize_factor != 1.0:
                    h_r = int(current_scene_raw_depth_data.shape[0] * resize_factor)
                    w_r = int(current_scene_raw_depth_data.shape[1] * resize_factor)

                    if resize_factor==0.5:
                        current_scene_raw_depth_data = mean_4pts(current_scene_raw_depth_data, h_r, w_r)
                    else:
                        print("resize_factor: ", resize_factor, "not implemented yet")
                        return

                np.save(temp, current_scene_raw_depth_data)
                created = True

        #copying grasps
        for user in users:
            id_user = user[0]
            GT_grasps = get_with_request(conn_gt, sql_request='SELECT * from grasps where scene_id=' + str(current_scene[0]) + " AND user_id =" + str(id_user))
            if resize_factor != 1.0:
                print("before GT_grasps: ", GT_grasps)
                GT_grasps_copy = []
                for g in GT_grasps:
                    g = list(g)
                    g[3] = resize_factor*g[3]
                    g[4] = resize_factor*g[4]
                    g[9] = resize_factor*g[9]
                    if resize_width:
                        g[10] = resize_factor*g[10]
                    GT_grasps_copy.append(g)
                GT_grasps = GT_grasps_copy
                print("after GT_grasps: ", GT_grasps)

            if len(GT_grasps) > 0:
                print("GT_grasps: ", GT_grasps)
                save_obj(GT_grasps, destination_object_name+str(id_user)+'_'+object_name+"_"+str(current_scene[0])+"_GT.pkl")
                with open(destination_object_name+str(id_user)+'_'+object_name+"_"+str(current_scene[0])+"_GT.txt", 'w+') as f:
                    for item in GT_grasps:
                        f.write("%s\n" % str(item))


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Export database')

    parser.add_argument('--string_to_replace', type=str, help='cf readme Export a sqlite database',
                        default='../data_files/cornell_dataset/')

    parser.add_argument('--replacement_string', type=str, help='cf readme Export a sqlite database',
                        default='../../data/data_files/cornell_dataset/')

    parser.add_argument('--resize_factor', type=float, choices=[1.0, 0.5],
                        help='if != 1.0 will export the database after downscaling it by a factor of resize_factor',
                        default=1.0)

    parser.add_argument('--resize_width', type=bool,
                        help='if the database is resized, whether to resize the width attributes of grasps',
                        default=True)

    parser.add_argument('--destination', type=str, help='path to the folder where the database will be exported ',
                        default='../../data/data_files/cornell_dataset_2/')

    parser.add_argument('--database_path', type=str, help='path to the sqlite database file that sould be read to be exported',
                        default='../../data/data_sqlite_files/cornell_sqlite.db')

    return parser.parse_args(argv)

if __name__ == '__main__':
    #python3 export_db.py --string_to_replace '../data_files/cornell_dataset' --replacement_string '../../data/data_files/cornell_dataset' --resize_factor 1.0 --resize_width True --destination '../../data/data_files/cornell_copy/' --database_path '../../data/data_sqlite_files/cornell_sqlite.db'
    #python3 export_db.py --string_to_replace '../data_files/our_dataset' --replacement_string '../../data/data_files/our_dataset' --resize_factor 1.0 --resize_width True --destination '../../data/data_files/our_copy/' --database_path '../../data/data_sqlite_files/our_dataset_sqlite.db'

    args = parse_arguments(sys.argv[1:])
    main(args)

