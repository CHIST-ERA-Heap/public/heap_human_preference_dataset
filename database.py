from scripts.heap_human_preference_dataset.database_util import create_sqlite_database, create_connection, get_with_request, insert_table, execute_with_request, get_object_id_with_name, get_object_name_with_id, get_scene_id_with_path
from scripts.common.grasps_util import depth_txt_to_numpy
import os
import cv2
import numpy as np
import configparser
from random import shuffle

#todo add camera config
class Database:
    def __init__(self, database_config_path, verbose=True):
        self.cfg = configparser.ConfigParser()
        if verbose:
            print('loading database_config_path: ',database_config_path)
        self.cfg.read(database_config_path)
        self.read_cfg()
        self.setup_database(verbose)

    def read_cfg(self):
        self.change_too_far_to_cam = self.cfg.getboolean('preprocessing','change_too_far_to_cam')
        self.too_far_to_cam_to_what = self.cfg.get('preprocessing','too_far_to_cam_to_what')
        self.too_far_to_cam_threshold = self.cfg.getfloat('preprocessing','too_far_to_cam_threshold')

        self.change_nans = self.cfg.getboolean('preprocessing','change_nans')
        self.nans_to_what = self.cfg.get('preprocessing','nans_to_what')

        self.change_too_close_to_cam = self.cfg.getboolean('preprocessing','change_too_close_to_cam')
        self.too_close_to_cam_to_what = self.cfg.get('preprocessing','too_close_to_cam_to_what')
        self.too_close_to_cam_threshold = self.cfg.getfloat('preprocessing','too_close_to_cam_threshold')

        self.sql_scene_request = self.cfg.get('database','sql_scene_request')
        self.database_path = self.cfg.get('database', 'database_path')
        self.database_candidates_path = self.cfg.get('database', 'database_candidates_path')
        self.GT_user = self.cfg.get('database','GT_user')
        self.GT_user_rectangle_eval = self.cfg.get('database','GT_user_rectangle_eval')
        self.string_to_replace = self.cfg.get('database', 'string_to_replace')
        self.replacement_string = self.cfg.get('database', 'replacement_string')

        self.path_to_scene_dir = self.cfg.get('database','path_to_scene_dir')

        users_and_generators_list_str = self.cfg.get('database', 'users_and_generators_list')
        users_and_generators_list_str = users_and_generators_list_str.split('\n')
        self.users_and_generators_list = [s for s in users_and_generators_list_str]
        print('self.users_and_generators_list: ', self.users_and_generators_list)

        self.default_object_name = self.cfg.get('database', 'default_object_name')
        if self.default_object_name == "None":
            self.default_object_name = None

    def setup_database(self, verbose=False):
        self.current_scene = None
        self.current_scene_rgb_image = None
        self.height, self.width, self.channels = 1280, 720, 3
        self.current_scene_raw_depth_data = None
        self.current_background_scene_rgb_image = None
        self.current_scene_mask = None

        self.dict_users_id = {}
        self.current_grasp_iterator = 0
        self.current_scene_iterator = 0
        self.grasps_candidates = {}#dict with the loaded grasp candidates key is scene id
        self.dict_rgb = {}
        self.dict_depth = {}
        self.dict_patches = {}
        self.dict_masks = {}
        self.dict_backgrounds = {}


        if not os.path.isfile(self.database_path):
            print(self.database_path, " does not exists")
            # create a database connection
            print('creating tables conn_gt')
            self.conn_gt = create_sqlite_database(self.database_path)
            users = get_with_request(self.conn_gt, sql_request='SELECT * FROM users')
            if self.GT_user not in users:
                insert_user = [self.GT_user]
                insert_table(self.conn_gt, insert_user, type='users', verbose=False)
                users = get_with_request(self.conn_gt, sql_request='SELECT * FROM users')


            self.all_scenes = []
        else:
            self.conn_gt = create_connection(self.database_path)
            #exemple: all_scenes:  [(1, 1, './our_dataset/originals/03-03-2021_162659', '_color.png', '_depth.npy', './our_dataset/backgrounds/empty_color.png'),
            # (91, 6, './our_dataset/originals/02-23-2021_141643', '_color.png', '_depth.npy', './our_dataset/backgrounds/empty_color.png')]
            users = get_with_request(self.conn_gt, sql_request='SELECT * FROM users')
            if self.GT_user not in users:
                insert_user = [self.GT_user]
                insert_table(self.conn_gt, insert_user, type='users', verbose=False)
                users = get_with_request(self.conn_gt, sql_request='SELECT * FROM users')

        for current_user in self.users_and_generators_list:
            if current_user not in users:
                insert_user = [current_user]
                insert_table(self.conn_gt, insert_user, type='users', verbose=False)

        users = get_with_request(self.conn_gt, sql_request='SELECT * FROM users')

        for user in users:
            if verbose:
                print('user: ', user)
            self.dict_users_id[user[1]] = user[0]
        print('self.dict_users_id: ', self.dict_users_id)

        self.load_all_scenes()

        execute_with_request(self.conn_gt,
                             sql_request='DELETE FROM grasps WHERE user_id=' + str(self.dict_users_id["success"]))

        execute_with_request(self.conn_gt,
                             sql_request='DELETE FROM grasps WHERE user_id=' + str(self.dict_users_id["failures"]))



        self.list_current_object = get_with_request(self.conn_gt, sql_request='SELECT * FROM objects')
        if verbose:
            print("all objects listed: ", self.list_current_object)

        if not os.path.isfile(self.database_candidates_path):
            # create a database connection
            print('creating tables conn_grasp_candidates')
            self.conn_grasp_candidates = create_sqlite_database(self.database_candidates_path)

            print('copying users')
            for user in users:
                insert_user = list(user[1:])
                insert_table(self.conn_grasp_candidates, insert_user, type='users', verbose=False)

            print('copying objects')
            for object in self.list_current_object:
                insert_object= list(object[1:])

                insert_table(self.conn_grasp_candidates, insert_object, type='objects', verbose=False)

            print('copying scenes')
            for scene in self.all_scenes:
                insert_scene= list(scene[1:])

                insert_table(self.conn_grasp_candidates, insert_scene, type='scenes', verbose=False)

            # query = "".join(line for line in self.conn_gt.iterdump())
            # # print("query: ")
            # # print(query)
            # # Dump old database in the new one.
            # self.conn_grasp_candidates.executescript(query)
            # self.conn_grasp_candidates.commit()

        else:
            self.conn_grasp_candidates = create_connection(self.database_candidates_path)


    def load_current_data(self, verbose=False):
        if verbose:
            print("load_current_data current_scene_iterator: ", self.current_scene_iterator)

        if self.current_scene_iterator >= len(self.all_scenes):
            self.current_scene_iterator = 0

        if self.current_scene_iterator < 0:
            self.current_scene_iterator = len(self.all_scenes) - 1

        self.current_scene = self.all_scenes[self.current_scene_iterator]
        self.current_scene_rgb_image, self.current_scene_raw_depth_data, self.current_background_scene_rgb_image, self.height, self.width, self.channels = self.get_rgb_depth_background_with_current_scene(self.current_scene)

        current_scene_id = self.current_scene[0]
        if current_scene_id in self.dict_masks:
            self.current_scene_mask = self.dict_masks[current_scene_id]
        else:
            print('setting current_scene_mask to None')
            self.current_scene_mask = None

        if verbose:
            print("self.current_scene: " + str(self.current_scene))
            print('self.height: ', self.height)
            print('self.width: ', self.width)
            print('self.channels: ', self.channels)

            print('self.current_scene_rgb_image: ', np.mean(self.current_scene_rgb_image))
            print('self.current_scene_raw_depth_data: ', np.mean(self.current_scene_raw_depth_data))

    def load_current_data_from_camera(self, temp_current_scene, latest_rgb_image, latest_aligned_depth, latest_background_rgb_image, verbose=False):
        if len(self.all_scenes) > 0:
            if self.all_scenes[-1][0] == -1:
                self.all_scenes[-1] = temp_current_scene
            else:
                self.all_scenes.append(temp_current_scene)

        else:
            self.all_scenes = [temp_current_scene]

        if -1 in self.dict_masks:
            # print('-1 in self.dict_masks')
            del self.dict_masks[-1]

        if -1 in self.dict_depth:
            # print('-1 in self.dict_depth')
            del self.dict_depth[-1]

        if -1 in self.dict_rgb:
            # print('-1 in self.dict_rgb')
            del self.dict_rgb[-1]

        if -1 in self.dict_backgrounds:
            # print('-1 in self.dict_backgrounds')
            del self.dict_backgrounds[-1]

        if -1 in self.grasps_candidates:
            # print('-1 in self.grasps_candidates')
            del self.grasps_candidates[-1]

        self.current_scene_iterator = len(self.all_scenes) - 1
        self.current_grasp_iterator = 0
        self.current_scene = self.all_scenes[self.current_scene_iterator]

        self.current_scene_rgb_image = latest_rgb_image
        self.height, self.width, self.channels = latest_rgb_image.shape

        self.current_scene_raw_depth_data = self.preprocess_depth(latest_aligned_depth)
        self.current_background_scene_rgb_image = latest_background_rgb_image
        self.current_scene_mask = None

        # self.current_scene_rgb_image, self.current_scene_raw_depth_data, self.current_background_scene_rgb_image, self.height, self.width, self.channels = self.get_rgb_depth_background_with_current_scene(self.current_scene)
        if verbose:
            print("self.current_scene: " + str(self.current_scene))
            print('self.height: ', self.height)
            print('self.width: ', self.width)
            print('self.channels: ', self.channels)

            print('self.current_scene_rgb_image: ', np.mean(self.current_scene_rgb_image))
            print('self.current_scene_raw_depth_data: ', np.mean(self.current_scene_raw_depth_data))


    def preprocess_depth(self, depth_raw, verbose=False):
        if verbose:
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))
        if self.change_too_far_to_cam:
            min = np.min(depth_raw)
            where_are_far = np.where(depth_raw > self.too_far_to_cam_threshold)
            depth_raw[where_are_far] = min
            if self.too_far_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_far_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_far_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_far_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_far] = value

        if self.change_nans:
            where_are_NaNs = np.isnan(depth_raw)
            if self.nans_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.nans_to_what == 'max':
                value = np.max(depth_raw)
            if self.nans_to_what == 'min':
                value = np.min(depth_raw)
            if self.nans_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_NaNs] = value

        if self.change_too_close_to_cam:
            where_are_close = np.where(depth_raw < self.too_close_to_cam_threshold)
            # where_are_close = np.where(depth_raw < 0.61)
            if self.too_close_to_cam_to_what == 'mean':
                value = np.mean(depth_raw)
            if self.too_close_to_cam_to_what == 'max':
                value = np.max(depth_raw)
            if self.too_close_to_cam_to_what == 'min':
                value = np.min(depth_raw)
            if self.too_close_to_cam_to_what == 'median':
                value = np.median(depth_raw)

            depth_raw[where_are_close] = value

        if verbose:
            print('after')
            print('depth_raw max: ', np.max(depth_raw))
            print('depth_raw mean: ', np.mean(depth_raw))
            print('depth_raw median: ', np.median(depth_raw))
            print('depth_raw min: ', np.min(depth_raw))

        depth_raw = np.reshape(depth_raw, (depth_raw.shape[0], depth_raw.shape[1], 1))

        return depth_raw

    def change_name_object(self, name=None):
        names = []
        self.list_current_object = get_with_request(self.conn_gt, sql_request='SELECT * FROM objects')
        for object in self.list_current_object:
            names.append(object[1])

        if name is None:
            print("******* list_of_objects *******")
            names.sort()
            for name in names:
                print(name)
            print("*******************************")
            name = input("Enter the new object_name: ")
        if name not in names:
            insert_object = [name]
            insert_table(self.conn_gt, insert_object, type='objects', verbose=False)
            insert_table(self.conn_grasp_candidates, insert_object, type='objects', verbose=False)

        id_object = get_object_id_with_name(self.conn_gt, name, verbose=False)
        id_current_scene = self.current_scene[0]
        print("new id_object: ", id_object)
        print("id_current_scene: ", id_current_scene)

        if id_current_scene == -1:

            path_scene = self.path_to_scene_dir + name + '/'
            if not os.path.exists(path_scene):
                print("creating: ", path_scene)
                os.makedirs(path_scene)
            path_scene = path_scene + name + '_'

            # TODO check that
            try:
                get_max_id_from_scene_request = 'SELECT * FROM scenes ORDER BY id DESC LIMIT 1'
                max_id = get_with_request(self.conn_gt, sql_request=get_max_id_from_scene_request)[0][0]
                test_nb = max_id + 1
            except:
                test_nb = 1
            print('test_new_id: ', test_nb)
            # test_rgb_path_exists = True
            # while test_rgb_path_exists:
            #     test_nb += 1
            #     test_rgb_path = path_scene + str(test_nb) + self.current_scene[3]
            #     test_rgb_path_exists = os.path.isfile(test_rgb_path)
            #     print("test_rgb_path_exists: ", test_rgb_path_exists)
            path_scene = path_scene + str(test_nb)

            insert_scene = [id_object, path_scene, self.current_scene[3], self.current_scene[4], self.current_scene[5]]
            insert_table(self.conn_gt, insert_scene, type='scenes', verbose=False)
            insert_table(self.conn_grasp_candidates, insert_scene, type='scenes', verbose=False)
            # todo ajust
            cv2.imwrite(path_scene + self.current_scene[3], self.current_scene_rgb_image)
            np.save(path_scene + self.current_scene[4], self.current_scene_raw_depth_data)

            id_scene = get_scene_id_with_path(self.conn_gt, path_scene, verbose=False)
            self.all_scenes[self.current_scene_iterator] = [id_scene, id_object, path_scene, self.current_scene[3],
                                                            self.current_scene[4], self.current_scene[5]]
            self.current_scene = self.all_scenes[self.current_scene_iterator]

            print("new scene: : ", self.all_scenes[self.current_scene_iterator])

        else:
            execute_with_request(self.conn_gt,
                                 sql_request='UPDATE scenes SET object_id = ' + str(id_object) + ' WHERE id=' + str(
                                     id_current_scene))
            # move?
        # todo check
        self.load_all_scenes()

    def load_all_scenes(self, remove_unique_object_scenes: object = False,
                        keep_only_scenes_without_good_bad: object = False,
                        keep_only_scenes_with_good_bad: object = False,
                        remove_unique_labeled_object_scenes: object = False,
                        verbose = False):

        all_scenes = get_with_request(self.conn_gt, self.sql_scene_request)
        all_scenes.sort(key=lambda x: x[1])
        if verbose:
            print("from load_all_scenes len all_scenes: ", len(all_scenes))

        if remove_unique_object_scenes:
            cleaned_scenes = []
            dict_obj_count = {}
            for s in all_scenes:
                if s[1] not in dict_obj_count:
                    dict_obj_count[s[1]] = 0
                dict_obj_count[s[1]] += 1
            for s in all_scenes:
                if dict_obj_count[s[1]] >= 2:
                    cleaned_scenes.append(s)
            if verbose:
                print("remove_unique_object_scenes len cleaned_scenes: ", len(cleaned_scenes))

            all_scenes = cleaned_scenes
        if keep_only_scenes_without_good_bad:
            id_user_GT = self.dict_users_id[self.GT_user]
            cleaned_scenes = []
            for s in all_scenes:
                GT = get_with_request(self.conn_gt,
                                      sql_request='SELECT * from grasps where user_id =' + str(
                                          id_user_GT) + ' AND scene_id=' + str(
                                          s[0]))
                nb_of_positive = 0
                nb_of_negative = 0
                for gt in GT:
                    if gt[11] > 0:
                        nb_of_positive += 1
                    else:
                        nb_of_negative += 1
                # print("s[0] ", s[0], " nb_of_positive: ", nb_of_positive, " nb_of_negative: ", nb_of_negative)
                if nb_of_positive == 0 or nb_of_negative == 0:
                    cleaned_scenes.append(s)
            if verbose:
                print("keep_only_scenes_without_good_bad len cleaned_scenes: ", len(cleaned_scenes))
            all_scenes = cleaned_scenes

        if keep_only_scenes_with_good_bad:
            id_user_GT = self.dict_users_id[self.GT_user]
            cleaned_scenes = []
            for s in all_scenes:
                GT = get_with_request(self.conn_gt,
                                      sql_request='SELECT * from grasps where user_id =' + str(
                                          id_user_GT) + ' AND scene_id=' + str(
                                          s[0]))
                nb_of_positive = 0
                nb_of_negative = 0
                for gt in GT:
                    if gt[11] > 0:
                        nb_of_positive += 1
                    else:
                        nb_of_negative += 1
                # print("s[0] ", s[0], " nb_of_positive: ", nb_of_positive, " nb_of_negative: ", nb_of_negative)
                if nb_of_positive > 0 and nb_of_negative > 0:
                    cleaned_scenes.append(s)
            if verbose:
                print("keep_only_scenes_with_good_bad len cleaned_scenes: ", len(cleaned_scenes))
            all_scenes = cleaned_scenes

        if remove_unique_labeled_object_scenes:
            cleaned_scenes = []
            dict_obj_count = {}
            for s in all_scenes:
                if s[1] not in dict_obj_count:
                    dict_obj_count[s[1]] = 0
                dict_obj_count[s[1]] += 1
            for s in self.all_scenes:
                if dict_obj_count[s[1]] >= 2:
                    cleaned_scenes.append(s)
            if verbose:
                print("remove_unique_labeled_object_scenes len cleaned_scenes: ", len(cleaned_scenes))
            all_scenes = cleaned_scenes

        self.all_scenes = all_scenes

    # def save_grasp(self, conn, id_scene, x, y, theta, length, width, depth_min, label=0.0):
    #     user_id = self.dict_users_id[self.GT_user]
    #     insert_grasp = [id_scene, user_id, x, y, float(depth_min), 0.0, 0.0, theta, length, width, label]
    #
    #     insert_table(self.conn_gt, insert_grasp, type='grasps', verbose=False)
    #
    #     grasp_drawed = [-1, id_scene, user_id, x, y, depth_min, 0.0, 0.0, theta, length, width, label]
    #     return grasp_drawed

    def save_grasp(self, conn, insert_grasp, verbose=False):
        insert_table(conn, insert_grasp, type='grasps', verbose=verbose)

    def get_rgb_depth_background_with_current_scene(self, current_scene, verbose=False):
        current_scene_id = current_scene[0]
        if current_scene_id in self.dict_rgb:
            current_scene_rgb_image = self.dict_rgb[current_scene_id]

        if current_scene_id in self.dict_depth:
            current_scene_raw_depth_data = self.dict_depth[current_scene_id]

            # if current_scene_id in self.dict_masks:
            #     current_scene_mask = self.dict_masks[current_scene_id]

        if current_scene_id in self.dict_backgrounds:
            current_background_scene_rgb_image = self.dict_backgrounds[current_scene_id]
        else:

            base_grasp_name = current_scene[2]
            file_name_color = base_grasp_name + current_scene[3]
            path_background = None
            if not os.path.isfile(file_name_color):
                #todo this could cause trouble at some point
                base_grasp_name = current_scene[2].replace(self.string_to_replace, self.replacement_string)
                if current_scene[5] is not None:
                    path_background = current_scene[5].replace(self.string_to_replace, self.replacement_string)
                file_name_color = base_grasp_name + current_scene[3]

            file_name_depth = base_grasp_name + current_scene[4]
            if verbose:
                print("get_rgb_depth_background_with_current_scene")
                print("current_scene: ", current_scene)
                print("base_grasp_name: ", base_grasp_name)
                print("file_name_color: ", file_name_color)
                print("file_name_depth_npy: ", file_name_depth)
                print("exists: ", os.path.isfile(file_name_depth))

            if not os.path.isfile(file_name_depth):
                print("scene ", current_scene, " not found")
                remove_answer = input("remove from database y/n? ")
                if remove_answer=='y':
                    sql_scene_command = 'DELETE from grasps WHERE scene_id = ' + str(current_scene[0])

                    execute_with_request(self.conn_gt,
                                         sql_request=sql_scene_command)
                    execute_with_request(self.conn_grasp_candidates,
                                         sql_request=sql_scene_command)

                    sql_scene_command = 'DELETE from scenes WHERE id = ' + str(current_scene[0])
                    execute_with_request(self.conn_gt,
                                         sql_request=sql_scene_command)
                    execute_with_request(self.conn_grasp_candidates,
                                         sql_request=sql_scene_command)


            if not os.path.isfile(file_name_color):
                print("current_scene: ", current_scene)
                print("base_grasp_name: ", base_grasp_name)
                print("file_name_color: ", file_name_color)
                print("file_name_depth_npy: ", file_name_depth)
                print(file_name_color, " exists: ", os.path.isfile(file_name_color))


            current_scene_rgb_image = cv2.imread(file_name_color)

            if path_background is not None:
                current_background_scene_rgb_image = cv2.imread(path_background)
            else:
                current_background_scene_rgb_image = None

            # if verbose:
            #     print("current_scene_rgb_image.shape: ", current_scene_rgb_image.shape)
            height, width, channels = current_scene_rgb_image.shape

            if current_scene[4] == '.txt':
                # with_npy = file_name_depth.replace('.txt','_depth.npy')
                with_npy = file_name_depth.replace('.txt','.npy')
                if os.path.isfile(with_npy):
                    current_scene_raw_depth_data = np.load(with_npy)
                else:
                    #we save the transormed file for cornell because this
                    current_scene_raw_depth_data = depth_txt_to_numpy(file_name_depth, (height, width))
                    np.save(with_npy, current_scene_raw_depth_data)
            elif current_scene[4][-4:] == '.npy':
                # try:
                current_scene_raw_depth_data = np.load(file_name_depth)
                # except Exception as e:
                #     print(e)
                #     current_scene_raw_depth_data = np.load(file_name_depth, allow_pickle=True)

            else:
                print("no method to load depth extension")
                exit()


            current_scene_raw_depth_data = self.preprocess_depth(current_scene_raw_depth_data)

            self.dict_rgb[current_scene_id] = current_scene_rgb_image
            self.dict_depth[current_scene_id] = current_scene_raw_depth_data
            # self.dict_masks[current_scene_id] = current_scene_mask
            self.dict_backgrounds[current_scene_id] = current_background_scene_rgb_image

        try:
            height, width, channels = current_scene_rgb_image.shape
        except Exception as e:
            print('Error: ', e)
            print("current_scene: ", current_scene)

        return current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels

    def get_id_of_GT_user(self):
        id_user = self.dict_users_id[self.GT_user]
        return id_user

    def get_grasps_with_request(self, sql_request):

        GT_grasps = self.get_gt_request(self.conn_gt, sql_request)
        return GT_grasps

    def get_gt_request(self, conn_gt, sql_request, b_force_angle =True, verbose=False):
        # print("get_gt_request")
        GT_grasps = get_with_request(conn_gt, sql_request, verbose=verbose)

        if b_force_angle:
            for i in range(len(GT_grasps)):
                GT_grasps[i] = list(GT_grasps[i])
                current_angle = GT_grasps[i][8]
                abs_value = np.pi / 2
                while current_angle > abs_value or current_angle < -abs_value:
                    if current_angle > abs_value:
                        # print("FAIL angle: ", current_angle)
                        current_angle = -(abs_value * 2 - current_angle)

                    if current_angle < -abs_value:
                        current_angle = (abs_value * 2 - abs(current_angle))
                GT_grasps[i][8] = current_angle
        return GT_grasps

    def remove_temporary_gt(self):
        execute_with_request(self.conn_gt,
                             sql_request='DELETE FROM grasps WHERE user_id=' + str(self.dict_users_id["success"]))

        execute_with_request(self.conn_gt,
                             sql_request='DELETE FROM grasps WHERE user_id=' + str(self.dict_users_id["failures"]))

    def execute_with_request(self, conn_gt, sql_request):
        execute_with_request(conn_gt, sql_request)

    def get_with_request(self, conn_gt, sql_request, verbose=False):
        return get_with_request(conn_gt, sql_request, verbose)