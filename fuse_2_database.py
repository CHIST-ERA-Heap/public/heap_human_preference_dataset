import os
from database_util import create_connection, get_with_request, get_object_name_with_id, create_sqlite_database, \
    insert_table, get_newest_id, get_user_id_with_name, get_object_id_with_name, get_scene_id_with_path
import numpy as np
import pickle
from export_db import depth_txt_to_numpy
import cv2
import sys
import argparse


# todo defined twice
def save_obj(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


# todo defined twice
def get_rgb_depth_background_with_current_scene(current_scene, verbose=True, string_to_replace='',
                                                replacement_string=''):
    base_grasp_name = current_scene[2]
    file_name_color = base_grasp_name + current_scene[3]
    if not os.path.isfile(file_name_color):
        print("before: ", current_scene[2])
        print("string_to_replace: ", string_to_replace)
        base_grasp_name = current_scene[2].replace(string_to_replace, replacement_string)
        print("after: ", base_grasp_name)

        file_name_color = base_grasp_name + current_scene[3]
    if not os.path.isfile(file_name_color):
        print(file_name_color, ' does not exists')
        exit()

    file_name_depth = base_grasp_name + current_scene[4]
    if verbose:
        print("current_scene: ", current_scene)
        print("base_grasp_name: ", base_grasp_name)
        print("file_name_color: ", file_name_color)
        print("file_name_depth_npy: ", file_name_depth)
        print("exists: ", os.path.isfile(file_name_depth))

    current_scene_rgb_image = cv2.imread(file_name_color)
    # print("current_scene[5]: ", current_scene[5])
    current_background_scene_rgb_image_path = current_scene[5]
    if current_background_scene_rgb_image_path is not None:
        # current_background_scene_rgb_image_path = current_scene[5].replace('./', './data_files/')
        if not os.path.isfile(current_background_scene_rgb_image_path):
            print("before: ", current_background_scene_rgb_image_path[2])
            print("string_to_replace: ", string_to_replace)
            current_background_scene_rgb_image_path = current_background_scene_rgb_image_path.replace(string_to_replace,
                                                                                                      replacement_string)
            print("after: ", current_background_scene_rgb_image_path)

        current_background_scene_rgb_image = cv2.imread(current_background_scene_rgb_image_path)
    else:
        current_background_scene_rgb_image = None

    height, width, channels = current_scene_rgb_image.shape
    if verbose:
        print("current_scene_rgb_image.shape: ", current_scene_rgb_image.shape)

    if current_scene[4] == '.txt':
        # with_npy = file_name_depth.replace('.txt','_depth.npy')
        with_npy = file_name_depth.replace('.txt', '.npy')
        if os.path.isfile(with_npy):
            current_scene_raw_depth_data = np.load(with_npy)
        else:
            # we save the transormed file for cornell because this
            current_scene_raw_depth_data = depth_txt_to_numpy(file_name_depth, (height, width))
            np.save(with_npy, current_scene_raw_depth_data)
    elif current_scene[4][-4:] == '.npy':
        current_scene_raw_depth_data = np.load(file_name_depth)
    else:
        print("no method to load depth extension")
        exit()
    return current_scene_rgb_image, current_scene_raw_depth_data, current_background_scene_rgb_image, height, width, channels


def update_table_data(conn, table_name):
    data = get_with_request(conn, sql_request='SELECT * FROM ' + table_name)
    data_list = []
    dict_data = {}
    for d in data:
        data_list.append(d[1])
        dict_data[d[0]] = d[1]
    return data_list, data, dict_data


def copy_db_into(current_conn_gt, conn_to_copy_into, verbose=True):
    data = get_with_request(current_conn_gt, sql_request='SELECT * FROM users')
    dict_old_id_user_new_id_user = {}
    for user_data in data:
        user_id = user_data[0]
        user_name = user_data[1]
        insert_user = [user_name]
        if verbose:
            print('adding user: ', insert_user)

        insert_table(conn_to_copy_into, insert_user, type='users', verbose=False)
        # new_id = get_newest_id(conn_to_copy_into,'users')
        new_id = get_user_id_with_name(conn_to_copy_into, user_name)
        if verbose:
            print('new_id ', new_id)
        dict_old_id_user_new_id_user[user_id] = new_id

    data = get_with_request(current_conn_gt, sql_request='SELECT * FROM objects')
    dict_old_id_objects_new_id_objects = {}
    for object_data in data:
        object_id = object_data[0]
        object_name = object_data[1]

        insert_object = [object_name]
        if verbose:
            print('adding object: ', insert_object)

        insert_table(conn_to_copy_into, insert_object, type='objects', verbose=False)
        # new_id = get_newest_id(conn_to_copy_into,'users')
        new_id = get_object_id_with_name(conn_to_copy_into, object_name)
        if verbose:
            print('old_id ', object_id)
            print('new_id ', new_id)
        dict_old_id_objects_new_id_objects[object_id] = new_id

    data = get_with_request(current_conn_gt, sql_request='SELECT * FROM scenes')
    dict_old_id_scenes_new_id_scenes = {}
    for scene_data in data:
        '''
        id integer PRIMARY KEY,
        object_id integer NOT NULL,
        path text NOT NULL UNIQUE, 
        rgb_extension text,
        depth_extension text,
        background_path text,

        FOREIGN KEY (object_id) REFERENCES objects (id)
        '''
        scene_id = scene_data[0]
        object_id = scene_data[1]
        path = scene_data[2]

        insert_scene = [dict_old_id_objects_new_id_objects[object_id], path, scene_data[3], scene_data[4], scene_data[5]]
        if verbose:
            print('adding scene: ', insert_scene)

        insert_table(conn_to_copy_into, insert_scene, type='scenes', verbose=False)

        new_id = get_scene_id_with_path(conn_to_copy_into, path)
        if verbose:
            print('old_id ', scene_id)
            print('new_id ', new_id)
        dict_old_id_scenes_new_id_scenes[scene_id] = new_id

    data = get_with_request(current_conn_gt, sql_request='SELECT * FROM grasps')
    dict_old_id_grasps_new_id_grasps = {}
    for grasp_data in data:
        '''
        id integer PRIMARY KEY,
        scene_id integer NOT NULL,
        user_id integer NOT NULL,
        x real NOT NULL,
        y real NOT NULL,
        depth real NOT NULL,
        euler_x real NOT NULL,
        euler_y real NOT NULL,
        euler_z real NOT NULL,
        length real NOT NULL,
        width real NOT NULL,
        quality real NOT NULL,
        FOREIGN KEY (grasp_id) REFERENCES grasps (id),
        FOREIGN KEY (user_id) REFERENCES user (id)
        '''
        grasp_id = grasp_data[0]
        scene_id = grasp_data[1]
        user_id = grasp_data[2]

        insert_grasp = [dict_old_id_scenes_new_id_scenes[scene_id], dict_old_id_user_new_id_user[user_id], grasp_data[3], grasp_data[4], grasp_data[5],
                        grasp_data[6], grasp_data[7], grasp_data[8], grasp_data[9], grasp_data[10], grasp_data[11], grasp_data[12], grasp_data[13], grasp_data[14], grasp_data[15],dict_old_id_objects_new_id_objects[grasp_data[16]]]

        if verbose:
            print('adding grasp: ', insert_grasp)

        insert_table(conn_to_copy_into, insert_grasp, type='grasps', verbose=False)

        new_id = get_newest_id(conn_to_copy_into, table_name='grasps')
        if verbose:
            print('old_id ', grasp_id)
            print('new_id ', new_id)
        dict_old_id_grasps_new_id_grasps[grasp_id] = new_id


def main(args):
    sqlite_destination_path = args.sqlite_destination_path
    database_path_1 = args.database_path_1
    database_path_2 = args.database_path_2

    exit = False
    if os.path.isfile(database_path_1):
        print("database_path: ", database_path_1, " exists")
    else:
        print("database_path: ", database_path_1, " does not exists")
        exit = True

    if os.path.isfile(database_path_2):
        print("database_path: ", database_path_2, " exists")
    else:
        print("database_path: ", database_path_2, " does not exists")
        exit = True

    # if os.path.isfile(sqlite_destination_path):
    #     print("database_path: ", sqlite_destination_path, " already exists")
    #     exit = True

    if os.path.isfile(sqlite_destination_path):
        os.remove(sqlite_destination_path)

    if exit:
        exit()

    conn_gt_1 = create_connection(database_path_1)
    conn_gt_2 = create_connection(database_path_2)

    conn_gt = create_sqlite_database(sqlite_destination_path)

    copy_db_into(conn_gt_1, conn_gt)
    copy_db_into(conn_gt_2, conn_gt)

def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Fuse 2 database')

    parser.add_argument('--database_path_1', type=str,
                        help='path to the sqlite database file that sould be read to be exported',
                        default='../../data/data_sqlite_files/candidates_fuse_dataset_sqlite.db')


    parser.add_argument('--database_path_2', type=str,
                        help='path to the sqlite database file that sould be read to be exported',
                        default='../../data/data_sqlite_files/new_lgps_table_dataset_grasps_candidates_sqlite.db')

    parser.add_argument('--sqlite_destination_path', type=str,
                        help='path to the sqlite database file that sould be read to be exported',
                        default='../../data/data_sqlite_files/candidates_fused_dataset_sqlite.db')

    return parser.parse_args(argv)


if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])
    main(args)

