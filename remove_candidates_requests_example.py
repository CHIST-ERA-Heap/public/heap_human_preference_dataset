
import os
from database_util import create_connection, get_with_request, get_object_name_with_id, execute_with_request

import sys
import argparse


def main(args):
    database_path = args.database_path
    id_grasp_to_remove = args.id_grasp_to_remove

    if os.path.isfile(database_path):
        print("database_path: ", database_path, " exists")
    else:
        print("database_path: ", database_path, " does not exists")
        exit()
    conn_gt = create_connection(database_path)

    execute_with_request(conn_gt,
                         sql_request='DELETE FROM grasps WHERE user_id=' + str(id_grasp_to_remove))



def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='remove grasps candidates from a sql database file')

    parser.add_argument('--database_path', type=str,
                        default='/home/panda2/Documents/IIT/Latent_space_GP_Selector/data/data_sqlite_files/online_clutter_live_data_table_sqlite.db')

    #self.dict_users_id:  {'default': 1, 'success': 2, 'failures': 3, 'skel': 4, 'edge': 5, 'all': 6, 'Dexnet': 7, 'GGCNN': 8, 'GRCONVNET': 9, 'GPD': 10, 'Superquadrics': 11, '6doFGraspNet': 12, 'centers': 13, 'CVGrasp': 14}
    parser.add_argument('--id_grasp_to_remove', type=int,
                        default=8)

    return parser.parse_args(argv)

if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])
    main(args)



