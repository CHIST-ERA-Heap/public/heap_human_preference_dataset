
import os
from database_util import create_connection, get_with_request, insert_table

import sys
import argparse


def main(args):
    database_path = args.database_path
    if os.path.isfile(database_path):
        print("database_path: ", database_path, " exists")

        conn_gt = create_connection(database_path)

        # exemple: all_scenes:  [(1, 1, './our_dataset/originals/03-03-2021_162659', '_color.png', '_depth.npy', './our_dataset/backgrounds/empty_color.png'),
        # (91, 6, './our_dataset/originals/02-23-2021_141643', '_color.png', '_depth.npy', './our_dataset/backgrounds/empty_color.png')]
        users = get_with_request(conn_gt, sql_request='SELECT * FROM users')
        print("current user in database: ", users)

        if args.new_user_name not in users:
            print('insert ', args.new_user_name)
            insert_user = [args.new_user_name]
            insert_table(conn_gt, insert_user, type='users', verbose=False)
            users = get_with_request(conn_gt, sql_request='SELECT * FROM users')
            print("current user in database: ", users)
        else:
            print(args.new_user_name, " is already in database")
    else:
        print("database_path: ", database_path, " does not exists")


def parse_arguments(argv):
    parser = argparse.ArgumentParser(description='Get stats from a sql database file')

    parser.add_argument('--database_path', type=str, help='depth extension of the files to read',
                        default='../../data/data_sqlite_files/our_dataset_sqlite.db')

    parser.add_argument('--new_user_name', type=str, help='name of new user to add')

    return parser.parse_args(argv)

if __name__ == '__main__':

    args = parse_arguments(sys.argv[1:])
    main(args)



